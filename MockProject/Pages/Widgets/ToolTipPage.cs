﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MockProject.Pages.Widgets
{
    public class ToolTipPage : BasePage
    {
        public ToolTipPage(IWebDriver driver) : base(driver)
        {
        }
        #region Page Elements
        public Label LblToolTipPageTitle => new Label(By.ClassName("main-header"), driver);
        public ToolsTip BtnHover => new(By.XPath("//button[@id='toolTipButton']"), driver);
        public ToolsTip TxtHover => new(By.XPath("//input[@id='toolTipTextField']"), driver);
        public ToolsTip LnkContrary => new(By.XPath("//a[contains(text(),'Contrary')]"), driver);
        public ToolsTip Lnk11032 => new(By.XPath("//a[contains(text(),'1.10.32')]"), driver);
        public Label LblToolTipsText => new Label(By.CssSelector(".tooltip-inner"),driver);
        #endregion
        #region Page Actions
        public string GetPageTitle()
        {
            return LblToolTipPageTitle.Text;
        }
        /// <summary>
        /// Hover on button
        /// </summary>
        /// <returns></returns>
        public string HoverButton()
        {
            Thread.Sleep(500);
            BtnHover.HoverOverElement();
            Thread.Sleep(500);
            return LblToolTipsText.Text;
        }
        /// <summary>
        /// Hover on text button
        /// </summary>
        /// <returns></returns>
        public string HoverTextBox()
        {
            Thread.Sleep(500);
            TxtHover.HoverOverElement();
            Thread.Sleep(500);
            return LblToolTipsText.Text;
        }
        /// <summary>
        /// Hover on link contrary
        /// </summary>
        /// <returns></returns>
        public string HoverLinkContrary()
        {
            Thread.Sleep(500);
            LnkContrary.HoverOverElement();
            Thread.Sleep(500);
            return LblToolTipsText.Text;
        }
        /// <summary>
        /// Hover on link 1.10.32
        /// </summary>
        /// <returns></returns>
        public string HoverLink11032()
        {
            Thread.Sleep(500);
            Lnk11032.HoverOverElement();
            Thread.Sleep(500);
            return LblToolTipsText.Text;
        }
        #endregion

    }
}
