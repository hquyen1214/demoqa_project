﻿using CORE.Elements.SimpleElement;
using FluentAssertions;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Pages.Widgets
{
    public class AccordianPage:BasePage
    {
        public AccordianPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements
        public Label LblAccordianPageTitle => new Label(By.ClassName("main-header"), driver);
        public Button BtnWhatIsLoremIpsum => new Button(By.XPath("//div[@id='section1Heading']"), driver);
        public Button BtnWhereDoesItComeFrom => new Button(By.XPath("//div[@id='section2Heading']"), driver);
        public Button BtnWhyDoWeUseIt => new Button(By.XPath("//div[@id='section3Heading']"), driver);
        public Label LblWhatIsLoremIpsum => new Label(By.CssSelector("div[class='collapse show']"), driver);
        public Label LblWhereDoesItComeFrom => new Label(By.CssSelector("div[class='collapse show']"), driver);
        public Label LblWhyDoWeUseIt => new Label(By.CssSelector("div[class='collapse show']"), driver);
        #endregion

        #region Page Actions
        /// <summary>
        /// Get Accordian Page Title
        /// </summary>
        /// <returns></returns>
        public string GetAccordianPageTitle()
        {
            return LblAccordianPageTitle.Text;
        }
        /// <summary>
        /// BtnWhatIsLoremIpsum is clicked
        /// </summary>
        public void ClickBtnWhatIsLoremIpsum()
        {
            Thread.Sleep(1000);
            BtnWhatIsLoremIpsum.Click();
            Thread.Sleep(1000);
            BtnWhatIsLoremIpsum.Click();
            Thread.Sleep(3000);
            LblWhatIsLoremIpsum.Displayed.Should().Be(LblWhatIsLoremIpsum.Displayed, "No information found");

        }
        /// <summary>
        /// BtnWhereDoesItComeFrom is clicked
        /// </summary>
        public void ClickBtnWhereDoesItComeFrom()
        {
            Thread.Sleep(1000);
            BtnWhereDoesItComeFrom.Click();
            Thread.Sleep(3000);
            LblWhereDoesItComeFrom.Displayed.Should().Be(LblWhereDoesItComeFrom.Displayed, "No information found");
        }
        /// <summary>
        /// BtnWhyDoWeUseIt is clicked
        /// </summary>
        public void ClickBtnWhyDoWeUseIt()
        {
            Thread.Sleep(1000);
            BtnWhyDoWeUseIt.Click();
            Thread.Sleep(3000);
            LblWhyDoWeUseIt.Displayed.Should().Be(LblWhyDoWeUseIt.Displayed, "No information found");
        }

        #endregion
    }
}
