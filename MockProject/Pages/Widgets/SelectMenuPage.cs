﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace MockProject.Pages.Widgets
{
    public class SelectMenuPage : BasePage
    {
        public SelectMenuPage(IWebDriver driver) : base(driver)
        {
        }
        #region Page Elements
        public Label LblSelectMenuPageTitle => new Label(By.XPath("//div[text()='Select Menu']"), driver);

        /// <summary>
        /// GetItemOfSelectValue
        /// </summary>
        public Button CmbSelectValue => new Button(By.XPath("//div[contains(text(),'Select Option')]"), driver);
        public Label LblSelectValue => new Label(By.XPath("//div[contains(text(),'A root option')]"), driver);
        public Button BtnSelectOption => new Button(By.XPath("//div[text()='A root option']"), driver);

        /// <summary>
        /// GetItemOfSelectOne
        /// </summary>
        public Button CmbSelectOne => new Button(By.XPath("//div[contains(text(),'Select Title')]"), driver);
        public Button ItemSelectTitle => new Button(By.XPath(" //div[contains(text(),'Mrs.')]"), driver);
        public Label LblSelectOneValue => new Label(By.XPath("//div[contains(text(),'Mrs.')]"), driver);

        /// <summary>
        /// GetItemOfSelectOne
        /// </summary>
        public Button CmbSelectType => new Button(By.XPath("//select[@id='oldSelectMenu']"), driver);
        public Button BtnSelectType => new Button(By.XPath("//option[text()='Yellow']"), driver);
        public Label LblOldStyleSelectMenu => new Label(By.XPath("//option[@value='3']"), driver);
        public SelectMenu OptionSelect => new SelectMenu(By.XPath("//div[@class=' css-26l3qy - menu']//div[@class=' css-11unzgr']"), driver);
        public SelectMenu TxtMultiSelectDropDown => new SelectMenu(By.XPath("//div[text()='Select...']"), driver);
        public SelectMenu SelectGreen => new SelectMenu(By.XPath("//div[text()='Green']"), driver);
        public SelectMenu BtnReclickMultipleSelectDropDown => new SelectMenu(By.XPath("//option[@value='volvo']"), driver);
        public SelectMenu SelectRed => new SelectMenu(By.XPath("//div[text()='Red']"), driver);
        public SelectMenu SelectBlack => new SelectMenu(By.XPath("//div[text()='Black']"), driver);
        public SelectMenu SelectBlue => new SelectMenu(By.XPath("//div[text()='Blue']"), driver);
        public SelectMenu BtnCancelAllSelect => new SelectMenu(By.XPath("//div[@class=' css-tlfecz-indicatorContainer'][position()= floor(last() div 2)]"), driver);
        public SelectMenu BtnCancelBlue => new SelectMenu(By.XPath("//div[text()='Blue']/following-sibling::div"), driver);
        public SelectMenu BtnCancelGreen => new SelectMenu(By.XPath("//div[text()='Green']/following-sibling::div"), driver);
        public SelectMenu BtnCancelRed => new SelectMenu(By.XPath("//div[text()='Red']/following-sibling::div"), driver);
        public SelectMenu BtnCancelBlack => new SelectMenu(By.XPath("//div[text()='Black']/following-sibling::div"), driver);
        public SelectMenu TextOption => new SelectMenu(By.XPath("//div[text()='No options']"), driver);
        public SelectMenu TextTxbMultiSelectDrop => new SelectMenu(By.XPath("//b[text()='Multiselect drop down']/parent::p/following-sibling::div"), driver);
        public DropAndDrag BtnDragFrom => new DropAndDrag(By.XPath("//option[contains(text(),'Volvo')]"), driver);
        public DropAndDrag BtnDragTo2 => new DropAndDrag(By.XPath("//option[contains(text(),'Saab')]"), driver);
        public DropAndDrag BtnDragTo3 => new DropAndDrag(By.XPath("//option[@value='opel']"), driver);
        public DropAndDrag BtnDragTo4 => new DropAndDrag(By.XPath("//option[@value='audi']"), driver);
        public Button BtnFrom => new Button(By.XPath("//option[contains(text(),'Volvo')]"), driver);
        public Button BtnTo2 => new Button(By.XPath("//option[@value='saab']"), driver);
        public Button BtnTo3 => new Button(By.XPath("//option[@value='opel']"), driver);
        public Button BtnTo4 => new Button(By.XPath("//option[@value='audi']"), driver);
        #endregion

        #region Page Actions
        /// <summary>
        /// Select value
        /// </summary>
        /// <returns></returns>
        public string GetItemOfSelectValue()
        {
            CmbSelectValue.Click();
            BtnSelectOption.Click();
            return LblSelectValue.Text;
        }

        /// <summary>
        /// Select one
        /// </summary>
        /// <returns></returns>
        public string GetItemOfSelectOne()
        {
            CmbSelectOne.Click();
            ItemSelectTitle.Click();
            //Choose item
            return LblSelectOneValue.Text;
        }

        /// <summary>
        /// Select one
        /// </summary>
        /// <returns></returns>
        public string GetItemOfOldStyleSelectMenu()
        {
            CmbSelectType.Click();
            Thread.Sleep(2000);
            //Choose item
            CmbSelectType.Click();
            BtnSelectType.Click();
            return LblOldStyleSelectMenu.GetAttribute("value");
        }
        /// <summary>
        /// Get select menu page title
        /// </summary>
        /// <returns></returns>

        public string GetSelectMenuPageTitle()
        {
            return LblSelectMenuPageTitle.Text;
        }
        /// <summary>
        /// Click on 'MultiSelect drop down'
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage ClickMultiSelectDropDown()
        {
            Thread.Sleep(3000);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TxtMultiSelectDropDown.Locator));
            ScrollDown(270);
            TxtMultiSelectDropDown.Click();
            Thread.Sleep(3000);
            return this;
        }
        
        /// <summary>
        /// Click select all options into 'Multiselect Drop Down' list
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage SelectAllOption()
        {
            SelectGreen.Click();
            SelectBlue.Click();
            SelectBlack.Click();
            SelectRed.Click();
            Thread.Sleep(3000);
            return this;
        }

        /// <summary>
        /// Deselect all option in drop down
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage DeSelectAllOption()
        {
            wait.Until(ExpectedConditions.ElementExists(this.BtnCancelAllSelect.Locator));
            BtnCancelAllSelect.Click();
            Thread.Sleep(3000);
            return this;

        }
        /// <summary>
        /// Deselect 'Blue' option into 'Multiselect Drop Down' text box
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage DeSelectOneOption()
        {
            SelectGreen.Click();
            SelectBlue.Click();
            Thread.Sleep(1000);
            BtnCancelBlue.Click();
            Thread.Sleep(1000);           
            return this;
        }

        /// <summary>
        /// Get text is displayed when got all options into list options of 'Multiselect Drop Down'
        /// </summary>
        /// <returns></returns>
        public string GetTextOptions()
        {
            return TextOption.Text;
        }

        /// <summary>
        /// Get text into 'MultiSelect Drop Down' text box 
        /// </summary>
        /// <returns></returns>
        public string GetTextTxtMultiSelectDropDown()
        {
            return TxtMultiSelectDropDown.Text;
        }

        /// <summary>
        /// Get text into 'MultiSelect Drop Down' text box after deselect 'Blue' option
        /// </summary>
        /// <returns></returns>
        public string GetTextTxtMultiSelect()
        {
            BtnReclickMultipleSelectDropDown.Click();
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnReclickMultipleSelectDropDown.Locator));
            return TextTxbMultiSelectDrop.Text;
        }
        /// <summary>
        /// Select 2 item by draging the mouse
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage DropAndDragTwoItems()
        {
            BtnDragFrom.DragToDrop(BtnDragFrom.WrappedObject, BtnDragTo2.WrappedObject);
            return this;
        }
        /// <summary>
        /// Select 3 item by draging the mouse
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage DropAndDragThreeItems()
        {
            BtnDragFrom.DragToDrop(BtnDragFrom.WrappedObject, BtnDragTo3.WrappedObject);
            return this;
        }
        /// <summary>
        /// Select 4 item by draging the mouse
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage DropAndDragFourItems()
        {
            BtnDragFrom.DragToDrop(BtnDragFrom.WrappedObject, BtnDragTo4.WrappedObject);
            return this;
        }
        /// <summary>
        /// Select items using ctrl keyboard button
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage KeyBoardCtrl()
        {           
            BtnDragFrom.KeyUpCtrl();
            Thread.Sleep(2000);
            BtnFrom.Click();
            BtnDragTo3.KeyDownCtrl();
            BtnTo3.Click();
            return this;
        }
        /// <summary>
        /// Get value of the last selected item 
        /// </summary>
        /// <returns></returns>
        public Boolean GetValue()
        {
            if (BtnDragTo2.GetAttribute("value") == "Saab")
            {
                //DropAndDragTwoItem();
                return true;
            }
            else if (BtnDragTo3.GetAttribute("value") == "opel")
            {
                //DropAndDragThreeItem();
                return true;
            }
            else if (BtnDragTo4.GetAttribute("value") == "audi")
            {
                //DropAndDragFourItem();
                return true;
            }
            else if (BtnTo3.GetAttribute("value") == "audi")
            {
                // KeyBoardCtrl();
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}