﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Pages
{
    public class TabsPage : BasePage
    {
        public TabsPage(IWebDriver driver) : base(driver)
        {
        }
        #region Page Elements
        public Label LblTabsPageTitle => new Label(By.ClassName("main-header"), driver);
        public Tab TabsOrigin => new (By.XPath("//a[@id='demo-tab-origin']"), driver);
        public Tab TxtOrigin => new(By.XPath("//div[@id='demo-tabpane-origin']"), driver);
        public Tab TabsUse => new(By.XPath("//a[@id='demo-tab-use']"), driver);
        public Tab TxtUse => new(By.XPath("//div[@id='demo-tabpane-use']"), driver);
        public Tab TabsWhat => new(By.XPath("//a[@id='demo-tab-what']"), driver);
        public Tab TxtWhat => new(By.XPath("//div[@id='demo-tabpane-what']"), driver);
        public Tab TabsMore => new(By.XPath("//a[@aria-disabled='true']"), driver);

        #endregion

        #region Page Actions
        /// <summary>
        /// Get page title
        /// </summary>
        /// <returns></returns>
        public string GetPageTitle()
        {
            return LblTabsPageTitle.Text;
        }
        /// <summary>
        /// Click on tab origin
        /// </summary>
        public void ClickTabOrigin()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TabsOrigin.Locator));
            Thread.Sleep(3000);
            TabsOrigin.Click();
        }
        /// <summary>
        /// Check text in orgin tab is displayed when clicked
        /// </summary>
        /// <returns></returns>
        public Boolean TextOrigin()
        {
            if (TxtOrigin.Displayed)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Click on Use origin
        /// </summary>
        public TabsPage ClickTabUse()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TabsUse.Locator));
            Thread.Sleep(3000);
            TabsUse.Click();
            Thread.Sleep(3000);
            return this;

        }
        /// <summary>
        /// Check text in Use tab is displayed when clicked
        /// </summary>
        /// <returns></returns>
        public Boolean TextUse()
        {
            if (TxtUse.Displayed)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Click on What origin
        /// </summary>
        public TabsPage ClickTabWhat()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TabsWhat.Locator));
            Thread.Sleep(3000);
            TabsWhat.Click();
            Thread.Sleep(3000);
            return this;

        }
        /// <summary>
        /// Check text in What tab is displayed when clicked
        /// </summary>
        /// <returns></returns>
        public Boolean TextWhat()
        {
            if (TxtWhat.Displayed)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Check tabs more is disabled
        /// </summary>
        /// <returns></returns>
        public Boolean CheckTabsMoreDisabledDisplayed()
        {
            if (TabsMore.Displayed)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}

