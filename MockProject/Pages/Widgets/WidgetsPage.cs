﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Pages.Widgets
{
    public class WidgetsPage : BasePage
    {
        public WidgetsPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements
        public Menu BtnMenu => new Menu(By.XPath("//div[@class='element-list collapse show']//li[@id='item-7']"), driver);
        public Button BtnAccordian => new Button(By.XPath("//span[normalize-space()='Accordian']"), driver);
        public Button BtnTabs => new(By.XPath("//div[@class='element-list collapse show']//li[@id='item-5']"), driver);
        public Button BtnToolTip => new(By.XPath("//div[@class='element-list collapse show']//li[@id='item-6']"), driver);
        public Label btnAuto => new Label(By.XPath("//div[@class='element-list collapse show']//li[@id='item-1']"), driver);
        public Label LblWidgetsPageTitle => new Label(By.ClassName("main-header"), driver);
        public Button BtnSelectMenu => new Button(By.XPath("//span[normalize-space()='Select Menu']"), driver);
        public Button BtnDatePicker => new Button(By.XPath("//span[normalize-space()='Date Picker']"), driver);
        public Button BtnSlider => new Button(By.XPath("//span[normalize-space()='Slider']"), driver);
        public Button BtnProgressBar => new Button(By.XPath("//span[normalize-space()='Progress Bar']"), driver);

        #endregion

        #region Page Actions
        /// <summary>
        /// Main Project: NavigateToWidgetsPage 
        /// </summary>
        public string GetPageTitle()
        {
            return LblWidgetsPageTitle.Text;
        }
        /// <summary>
        /// Click on Menu button on left panel
        /// </summary>
        /// <returns></returns>
        public MenuPage ClickBtnMenu()
        {
            ScrollDown(1000);
            BtnMenu.Click();
            return new MenuPage(driver);
        }
        /// <summary>
        /// Click on Auto Complete button on left panel
        /// </summary>
        /// <returns></returns>
        public AutoCompletePage NavigateToAutoPage()
        {
            ScrollDown(300);
            btnAuto.Click();
            return new AutoCompletePage(driver);
        }

        /// <summary>
        /// continue project: NavigateToSelectMenuPage 
        /// </summary>
        public SelectMenuPage NavigateToSelectMenuPage()
        {
            ScrollDown(600);
            BtnSelectMenu.Click();
            return new SelectMenuPage(driver);
        }


        /// <summary>
        /// continue project: NavigateToDatePickerPage 
        /// </summary>
        public DatePickerPage NavigateToDatePickerPage()
        {
            //Have to move the mouse down to catch the button
            ScrollDown(300);
            BtnDatePicker.Click();
            return new DatePickerPage(driver);
        }

        /// <summary>
        /// Click button Slider
        /// </summary>
        /// <returns></returns>
        public AccordianPage NavigateToAccordianPage()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnAccordian.Locator));
            BtnAccordian.Click();
            return new AccordianPage(driver);
        }
        public TabsPage Tabs()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Navigate to tabs page
        /// </summary>
        /// <returns></returns>
        public TabsPage NavigateToTabsPage()
        {
            ScrollDown(600);
            BtnTabs.Click();
            return new TabsPage(driver);
        }
        public ToolTipPage NavigateToToolTipsPage()
        {
            ScrollDown(600);
            BtnToolTip.Click();
            return new ToolTipPage(driver);
        }
        /// <summary>
        /// Click button Slider
        /// </summary>
        /// <returns></returns>
        public SliderPage NavigateToSliderPage()
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript("window.scrollBy(0,300)");
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnSlider.Locator));
            BtnSlider.Click();
            return new SliderPage(driver);
        }
        /// <summary>
        /// Click button Progress Bar
        /// </summary>
        /// <returns></returns>
        public ProgressBarPage NavigateToProgressBarPage()
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript("window.scrollBy(0,300)");
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnProgressBar.Locator));
            BtnProgressBar.Click();
            return new ProgressBarPage(driver);
        }

        /// <summary>
        /// SelectMenuPage1
        /// </summary>
        /// <returns></returns>
        public SelectMenuPage ChooserSelect()
        {
            ScrollDown(600);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnSelectMenu.Locator));
            BtnSelectMenu.Click();
            return new SelectMenuPage(driver);
        }
        #endregion
    }
}

