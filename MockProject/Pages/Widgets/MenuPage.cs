﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Pages.Widgets
{
    public class MenuPage : BasePage
    {
        public MenuPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements      
        public Menu TxtMenu => new Menu(By.XPath("//div[@class='pattern-backgound playgound-header']"), driver);
        public Menu MnuMainItem1 => new Menu(By.XPath("//a[normalize-space()='Main Item 1']"), driver);
        public Menu MnuMainItem2 => new Menu(By.XPath("//a[normalize-space()='Main Item 2']"), driver);
        public Menu MnuMainItem3 => new Menu(By.XPath("//a[normalize-space()='Main Item 3']"), driver);
        public Menu MnuSubItem1 => new Menu(By.XPath("(//a[text()='Sub Item'])[position()= floor(last() div 2 + 0.5)]"), driver);
        public Menu MnuSubItem2 => new Menu(By.XPath("(//a[text()='Sub Item'])[last()]"), driver);
        public Menu SubSubList => new Menu(By.XPath("//a[text()='SUB SUB LIST »']"), driver);
        public Menu SubSubItem1 => new Menu(By.XPath("//a[text()='Sub Sub Item 1']"), driver);
        public Menu SubSubItem2 => new Menu(By.XPath("//a[text()='Sub Sub Item 2']"), driver);
        public Menu CSSColorMainItem => new Menu(By.XPath("//ul[@id='nav']/li"), driver);
        public Menu CSSColorSubItem => new Menu(By.XPath("//div[@class='nav-menu-container']/ul[@id='nav']/li/ul/li"), driver);
        public Menu CSSColorSubSubItem => new Menu(By.XPath("//div[@id='app']//li//li//li"), driver);
        #endregion

        #region Page Actions
        /// <summary>
        ///  Mouse hover over "Main Item 1"
        /// </summary>
        /// <returns></returns>
        public MenuPage HoverMainItem1()
        {
            //Hover over MainItem 1 
            MnuMainItem1.MoveToMiddleElement();
            MnuMainItem1.Click();
            return this;
        }

        /// <summary>
        /// Mouse hover over "Main Item 2"
        /// </summary>
        /// <returns></returns>
        public MenuPage HoverMainItem2()
        {
            //Hover over MainItem 2           
            MnuMainItem2.MoveToMiddleElement();
            MnuMainItem2.Click();
            return this;
        }

        /// <summary>
        ///  Mouse hover over "Main Item 3"
        /// </summary>
        /// <returns></returns>
        public MenuPage HoverMainItem3()
        {
            MnuMainItem3.MoveToMiddleElement();
            MnuMainItem3.Click();
            return this;
        }

        /// <summary>
        /// Mouse hover over "Sub Item 1"
        /// </summary>
        /// <returns></returns>
        public MenuPage HoverSubItem1()
        {
            MnuSubItem1.MoveToMiddleElement();
            MnuSubItem1.Click();
            return this;
        }

        /// <summary>
        /// Mouse hover over "Sub Item 2"
        /// </summary>
        /// <returns></returns>
        public MenuPage HoverSubItem2()
        {
            MnuSubItem2.MoveToMiddleElement();
            MnuSubItem2.Click();
            return this;
        }

        /// <summary>
        /// Mouse hover over sub menu option "SUB SUB LIST"
        /// </summary>
        /// <returns></returns>
        public MenuPage HoverSubSubList()
        {
            SubSubList.MoveToMiddleElement();
            SubSubList.Click();
            return this;
        }

        /// <summary>
        /// Mouse hover over "sub sub item 1"
        /// </summary>
        /// <returns></returns>
        public MenuPage HoverSubSubItem1()
        {
            SubSubItem1.MoveToMiddleElement();
            SubSubItem1.Click();
            return this;
        }

        /// <summary>
        /// Mouse hover over "sub sub item 2"
        /// </summary>
        /// <returns></returns>
        public MenuPage HoverSubSubItem2()
        {
            SubSubItem2.MoveToMiddleElement();
            SubSubItem2.Click();
            return this;
        }
        /// <summary>
        /// Get Menu page title
        /// </summary>
        /// <returns></returns>
        public string MenuText()
        {
            return TxtMenu.Text;
        }
        /// <summary>
        /// Get Main Item color
        /// </summary>
        /// <returns></returns>
        public string MainItemColor()
        {
            return CSSColorMainItem.GetCssValue("color");
        }
        /// <summary>
        /// Set Sub Item color
        /// </summary>
        /// <returns></returns>
        public string SubItemColor()
        {
            return CSSColorSubItem.GetCssValue("color");
        }
        /// <summary>
        /// Get sub sub item color
        /// </summary>
        /// <returns></returns>
        public string SubSubItemColor()
        {
            return CSSColorSubSubItem.GetCssValue("color");
        }
        #endregion
    }
}
