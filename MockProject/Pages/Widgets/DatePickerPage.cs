using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MockProject.Pages.Widgets
{
    public class DatePickerPage : BasePage
    {
        public DatePickerPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements
        public Label LblDatePickerPageTitle => new Label(By.ClassName("main-header"), driver);

        public Textbox TxtValue => new Textbox(By.XPath("//input[@id='datePickerMonthYearInput']"), driver);
       
        public Label LblDisplayedValue => new Label(By.XPath("//input[@id='datePickerMonthYearInput']"), driver);

        ///Element to click Date
        public Button BtnMonth => new Button(By.XPath(" //select[@class='react-datepicker__month-select']"), driver);

        public Label LblMonth => new Label(By.XPath("//option[@value='1']"), driver);
        public Button BtnYear => new Button(By.XPath("//select[@class='react-datepicker__year-select']"), driver);
        public Label LblYear => new Label(By.XPath("//option[@value='2000']"), driver);
        public Label LblDay => new Label(By.XPath("//div[contains(@aria-label,'Choose Tuesday, February 8th, 2000')]"), driver);

        ///Element to click Date and Time
        public Textbox TxtDateAndTime => new Textbox(By.XPath("//input[@id='dateAndTimePickerInput']"), driver);

        public Button BtnMonth1 => new Button(By.XPath("//span[@class='react-datepicker__month-read-view--selected-month']"), driver);

        public Label LblMonth1 => new Label(By.XPath("//div[contains(@class,'react-datepicker__header__dropdown react-datepicker__header__dropdown--scroll')]//div[6]"), driver);
        public Button BtnYear1 => new Button(By.XPath("//span[@class='react-datepicker__year-read-view--selected-year']"), driver);
        public Label LblYear1 => new Label(By.XPath("//div[@class='react-datepicker__year-option react-datepicker__year-option--selected_year']"), driver);
        public Label LblDay1 => new Label(By.XPath("//div[@aria-label='Choose Tuesday, June 8th, 2021']"), driver);
        public Label LblTime => new Label(By.XPath("//li[normalize-space()='03:00']"), driver);
        public Label LblDateAndTimeValue => new Label(By.XPath("//input[@id='dateAndTimePickerInput']"), driver);

        #endregion

        #region Page Actions
        /// <summary>
        /// Check that have reached the correct page required
        /// </summary>
        /// <returns> Title Page </returns>
        public string GetSelectMenuPageTitle()
        {
            return LblDatePickerPageTitle.Text;
        }

        /// <summary>
        /// check that the displayed value is equal to the input value
        /// </summary>
        /// <returns></returns>
        public string GetDisplayedValueAfterClick()
        {
            TxtValue.Click();
            BtnMonth.Click();
            LblMonth.Click();
            BtnYear.Click();
            LblYear.Click();
            LblDay.Click();
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.LblDisplayedValue.Locator));
            return LblDisplayedValue.GetAttribute("value");
        }


        /// <summary>
        /// check that the displayed value is equal to the input value
        /// </summary>
        /// <returns></returns>
        public string GetDisplayedDateAndTimeAfterClick()
        {
            TxtDateAndTime.Click();
            BtnMonth1.Click();
            LblMonth1.Click();
            BtnYear1.Click();
            LblYear1.Click();
            LblDay1.Click();
            LblTime.Click();
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.LblDisplayedValue.Locator));
            return LblDateAndTimeValue.GetAttribute("value");
        }

        /// <summary>
        /// check that the displayed value is equal to the input value
        /// </summary>
        /// <returns></returns>
        public string GetDisplayedValue()
        {
            Actions actions = new Actions(driver);
            WebElement LblDisplayedValue = (WebElement)keyword.FindElement(By.XPath("//input[@id='datePickerMonthYearInput']"));
            actions.DoubleClick(LblDisplayedValue).DoubleClick(LblDisplayedValue).Perform();
            LblDisplayedValue.SendKeys("date");
            LblDisplayedValue.SendKeys(Keys.Enter);
            return LblDisplayedValue.GetAttribute("value");
        }

        /// <summary>
        /// check that the displayed value is equal to the input value
        /// </summary>
        /// <returns></returns>
        public string GetDisplayedNumValue()
        {
            Actions actions = new Actions(driver);
            WebElement LblDisplayedValue = (WebElement)keyword.FindElement(By.XPath("//input[@id='datePickerMonthYearInput']"));
            actions.DoubleClick(LblDisplayedValue).DoubleClick(LblDisplayedValue).Perform();
            //Senkeys data
            LblDisplayedValue.SendKeys("11212021");
            LblDisplayedValue.SendKeys(Keys.Enter);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.LblDisplayedValue.Locator));
            return LblDisplayedValue.GetAttribute("value");
        }

        public string GetDisplayedDateValue()
        {
            Actions actions = new Actions(driver);
            WebElement LblDisplayedValue = (WebElement)keyword.FindElement(By.XPath("//input[@id='datePickerMonthYearInput']"));
            actions.DoubleClick(LblDisplayedValue).DoubleClick(LblDisplayedValue).Perform();
            //Senkeys data
            LblDisplayedValue.SendKeys("11/08/2021");
            LblDisplayedValue.SendKeys(Keys.Enter);
            //wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.LblDisplayedValue.Locator));
            return LblDisplayedValue.GetAttribute("value");
        }

        /// <summary>
        /// DateTime
        /// </summary>
        /// <returns></returns>
        public string GetDisplayedValueInDateTime()
        {
            Actions actions = new Actions(driver);
            WebElement LblDateTime = (WebElement)keyword.FindElement(By.XPath("//input[@id='dateAndTimePickerInput']"));
            Thread.Sleep(2000);
            actions.DoubleClick(LblDateTime).DoubleClick(LblDateTime).Perform();
            LblDateTime.SendKeys("date");
            LblDateTime.SendKeys(Keys.Enter);
            return LblDateTime.GetAttribute("value");
        }
        #endregion
    }
}

