﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Pages.Widgets
{
    public class ProgressBarPage : BasePage
    {
        public ProgressBarPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements
        public Label LblProgressBarPageTitle => new Label(By.ClassName("main-header"), driver);
        //Button Start Progress Bar page
        public Button BtnStartContinue => new Button(By.XPath("//button[@id='startStopButton']"), driver);
        //Button Reset Progress Bar page
        public Button BtnResetContinue => new Button(By.XPath("//button[@id='resetButton']"), driver);
        
        //Text box value Progress Bar page
        
        public ProgressBar TxbProgressBarvalue => new ProgressBar(By.XPath("//div[@role='progressbar']"), driver);
        #endregion

        #region Page Actions
        /// <summary>
        /// Get Progress Bar Page Title
        /// </summary>
        /// <returns></returns>
        public string GetProgressBarPageTitle()
        {
            return LblProgressBarPageTitle.Text;
        }
        /// <summary>
        /// click start
        /// </summary>
        /// <returns></returns>
        public ProgressBarPage ClickProgressBarStart()
        {
            Thread.Sleep(300);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnStartContinue.Locator));
            BtnStartContinue.Click();
            Thread.Sleep(15000);
            return new ProgressBarPage(driver);
        }
        /// <summary>
        /// Check 100%
        /// </summary>
        /// <returns></returns>
        public int AssertStart()
        {
            return TxbProgressBarvalue.GetValueProgressBar();
        }

        /// <summary>
        /// Click Stop
        /// </summary>
        /// <returns></returns>
        public ProgressBarPage ClickProgressBarStop()
        {
            Thread.Sleep(300);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnStartContinue.Locator));
            BtnStartContinue.Click();
            Thread.Sleep(5000);
            //wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnResetContinue.Locator));
            BtnStartContinue.Click();
            Thread.Sleep(3000);
            return new ProgressBarPage(driver);
        }
        /// <summary>
        /// Check %
        /// </summary>
        /// <returns></returns>
        public int AssertStop()
        {
            return TxbProgressBarvalue.GetValueProgressBar();
        }

        /// <summary>
        /// Click Reset
        /// </summary>
        /// <returns></returns>
        public ProgressBarPage ClickProgressBarReset()
        {
            Thread.Sleep(300);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnStartContinue.Locator));
            BtnStartContinue.Click();
            Thread.Sleep(15000);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnResetContinue.Locator));
            BtnResetContinue.Click();
            Thread.Sleep(3000);
            return new ProgressBarPage(driver);
        }
        /// <summary>
        /// Check 0%
        /// </summary>
        /// <returns></returns>
        public int AssertReset()
        {
            return TxbProgressBarvalue.GetValueProgressBar();
        }

        #endregion
    }
}
