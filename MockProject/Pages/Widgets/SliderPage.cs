﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Pages.Widgets
{
    public class SliderPage : BasePage
    {
        public SliderPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements
        public Label LblSliderPageTitle => new Label(By.ClassName("main-header"), driver);
        // Text box value slider page
        public Slider TxbSlidervalue => new Slider(By.XPath("//input[@id='sliderValue']"), driver);
        // Button Slider page
        public Slider BtnSlider => new Slider(By.XPath("//input[@type='range']"), driver);
        #endregion

        #region Page Actions
        /// <summary>
        /// Get Slider Page Title
        /// </summary>
        /// <returns></returns>
        public string GetSliderPageTitle()
        {
            return LblSliderPageTitle.Text;
        }
        /// <summary>
        /// Action slider 100
        /// </summary>
        /// <returns></returns>
        public SliderPage ActionsSlider100()
        {
            BtnSlider.ActionSlider(BtnSlider.Size.Width);
            return new SliderPage(driver);
        }
        /// <summary>
        /// Check 100
        /// </summary>
        /// <returns></returns>
        public int Assert100()
        {
            return TxbSlidervalue.GetValue();
        }
        /// <summary>
        /// Action slider 0
        /// </summary>
        /// <returns></returns>
        public SliderPage ActionsSlider0()
        {
            BtnSlider.ActionSlider(-BtnSlider.Size.Width);
            return new SliderPage(driver);

        }
        /// <summary>
        /// Check 0
        /// </summary>
        /// <returns></returns>
        public int Assert0()
        {
            return TxbSlidervalue.GetValue();
        }
        /// <summary>
        /// Action slider 50
        /// </summary>
        /// <returns></returns>
        public SliderPage ActionsSlider50()
        {
            BtnSlider.ActionSlider(0);
            return new SliderPage(driver);
        }
        /// <summary>
        /// Check 50
        /// </summary>
        /// <returns></returns>
        public int Assert50()
        {
            return TxbSlidervalue.GetValue();
        }
        #endregion
    }
}
