﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Pages.Widgets
{
    public class AutoCompletePage : BasePage
    {
        public AutoCompletePage(IWebDriver driver) : base(driver)
        {
        }
        #region Page Elements
        public Label LblAutoCompletePageTitle => new Label(By.ClassName("main-header"), driver);
        public Textbox TxbAutoCompleteMultilColor => new Textbox(By.CssSelector("#autoCompleteMultipleInput"), driver);
        public Textbox TxbAutoCompleteSingleColor => new Textbox(By.CssSelector("#autoCompleteSingleInput"), driver);
        public Textbox TxbAutoCompleteMultilColorContainer => new Textbox(By.CssSelector("#autoCompleteMultipleContainer"), driver);
        public Textbox TxbAutoCompleteSingleColorContainer => new Textbox(By.CssSelector("#autoCompleteSingleContainer"), driver);

        #endregion

        #region Page Action
        /// <summary>
        /// Get page title
        /// </summary>
        /// <returns></returns>
        public string GetPageTitle()
        {
            return LblAutoCompletePageTitle.Text;
        }
        /// <summary>
        /// input multi colors into text box
        /// </summary>
        /// <returns></returns>
        public string TxbAucomplete()
        {
            TxbAutoCompleteMultilColor.Click();
            TxbAutoCompleteMultilColor.SendKeys("Red");
            TxbAutoCompleteMultilColor.SendKeys(Keys.Enter);
            TxbAutoCompleteMultilColor.SendKeys("Blue");
            TxbAutoCompleteMultilColor.SendKeys(Keys.Enter);
            TxbAutoCompleteMultilColor.SendKeys("Yellow");
            TxbAutoCompleteMultilColor.SendKeys(Keys.Enter);
            LblAutoCompletePageTitle.Click();
            return TxbAutoCompleteMultilColorContainer.GetMessage();
        }
        /// <summary>
        /// Use auto fill to input multi colors into text box
        /// </summary>
        /// <returns></returns>
        public string TxbAucompleteautofil()
        {
            TxbAutoCompleteMultilColor.Click();
            TxbAutoCompleteMultilColor.SendKeys("R");
            TxbAutoCompleteMultilColor.SendKeys(Keys.Enter);
            TxbAutoCompleteMultilColor.SendKeys("B");
            TxbAutoCompleteMultilColor.SendKeys(Keys.Enter);
            TxbAutoCompleteMultilColor.SendKeys("Y");
            TxbAutoCompleteMultilColor.SendKeys(Keys.Enter);
            LblAutoCompletePageTitle.Click();
            return TxbAutoCompleteMultilColorContainer.GetMessage();
        }
        /// <summary>
        /// Input SINGLE color
        /// </summary>
        /// <returns></returns>
        public string TxbAucompletesingle()
        {
            TxbAutoCompleteSingleColor.Click();
            TxbAutoCompleteSingleColor.SendKeys("Red");
            TxbAutoCompleteSingleColor.SendKeys(Keys.Enter);
            LblAutoCompletePageTitle.Click();
            return TxbAutoCompleteSingleColorContainer.GetMessage();
        }
        /// <summary>
        /// Use auto fill to input SINGLE color
        /// </summary>
        /// <returns></returns>
        public string TxbAucompletesingleautofil()
        {
            TxbAutoCompleteSingleColor.Click();
            TxbAutoCompleteSingleColor.SendKeys("R");
            TxbAutoCompleteSingleColor.SendKeys(Keys.Enter);
            LblAutoCompletePageTitle.Click();
            return TxbAutoCompleteSingleColorContainer.GetMessage();
        }
        #endregion
    }
}
