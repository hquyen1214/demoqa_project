﻿using CORE.Elements.SimpleElement;
using MockProject.Pages.Widgets;
using MockProject.Pages.Elements;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;



namespace MockProject.Pages
{
    public class Homepage : BasePage
    {
        public Homepage(IWebDriver driver) : base(driver)
        {
        }
        #region Page Elements
        public Button BtnElementCard => new Button(By.XPath("//h5[normalize-space()='Elements']/parent::*/parent::*/parent::*"), driver);
        public Button BtnWidgetsCard => new Button(By.XPath("//h5[normalize-space()='Widgets']/parent::*/parent::*/parent::*"), driver);
        #endregion

        #region Page Actions
        /// <summary>
        /// Navigate to Elements Page
        /// </summary>
        /// <returns></returns>
        public ElementsPage NavigateToElementsPage()
        {
            ScrollDown(200);
            BtnElementCard.Click();
            return new ElementsPage(driver);
        }
        /// <summary>
        /// Navigate to Widgets Page
        /// </summary>
        /// <returns></returns>
        public WidgetsPage NavigateToWidgetsPage()
        {
            ScrollDown(300);
            BtnWidgetsCard.Click();
            return new WidgetsPage(driver);
        }
        #endregion
    }

}