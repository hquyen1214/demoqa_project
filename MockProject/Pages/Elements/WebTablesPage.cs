﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;

namespace MockProject.Pages.Elements
{
    public class WebTablesPage : BasePage
    {
        public WebTablesPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements
        public Button BtnEditSelector => new Button(By.XPath("//div[@class='action-buttons']/span[@title='Edit'][@id='edit-record-1']"),driver);

        public Button BtnDeleteSelector => new Button(By.XPath("//div[@class='action-buttons']/span[@title='Delete'][@id='delete-record-1']"),driver);

        public string FirstValue = "(//div[@class='rt-tr -odd']/div[@class='rt-td'][1])[1]";

        public string LblFirstName = "//div[@class='rt-resizable-header-content'][contains(text(),'First Name')]";

        public Button BtnAdd => new Button(By.XPath("//button[@id='addNewRecordButton']"), driver);

        public Button BtnSubmit => new Button(By.XPath("//button[@id='submit']"),driver);
        
        public Textbox TxtFristName => new Textbox(By.XPath("//input[@id='firstName']"), driver);
        public Textbox TxtLastName => new Textbox(By.XPath("//input[@id='lastName']"), driver);
        public Textbox TxtEmail => new Textbox(By.XPath("//input[@id='userEmail']"), driver);
        public Textbox TxtAge => new Textbox(By.XPath(" //input[@id='age']"), driver);
        public Textbox TxtSalary => new Textbox(By.XPath("//input[@id='salary']"), driver);
        public Textbox TxtDepartment => new Textbox(By.XPath("//input[@id='department']"), driver);
        public Textbox TxtSearchText => new Textbox(By.XPath("//input[@id='searchBox']"), driver);
        
        public Table Table => new Table(By.ClassName("rt-table"), driver);

        public IWebElement addModal => driver.FindElement(By.ClassName("modal-content"));

        public IList<IWebElement> FindMultiElement()
            => driver.FindElements(By.XPath($"//*[@class='rt-td'][1]"));
        #endregion

        #region Page Actions
        /// <summary>
        /// Set value for textbox when user want to add a new row
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="age"></param>
        /// <param name="salary"></param>
        /// <param name="department"></param>
        public void SendValuesToTextBox(
            string firstName,
            string lastName,
            string email,
            int age,
            double salary,
            string department)
        {
            TxtFristName.Clear();
            TxtFristName.SendKeys(firstName);

            TxtLastName.Clear();
            TxtLastName.SendKeys(lastName);

            TxtEmail.Clear();
            TxtEmail.SendKeys(email);

            TxtAge.Clear();
            TxtAge.SendKeys(age.ToString());

            TxtSalary.Clear();
            TxtSalary.SendKeys(salary.ToString());

            TxtDepartment.Clear();
            TxtDepartment.SendKeys(department);
        }
        
        /// <summary>
        /// Add new member for table
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="age"></param>
        /// <param name="salary"></param>
        /// <param name="department"></param>
        public void AddNewInfo(
            string firstName,
            string lastName,
            string email,
            int age,
            double salary,
            string department)
        {
            ScrollDown(10);
            BtnAdd.Click();
            SendValuesToTextBox(firstName, lastName, email, age, salary, department);
            BtnSubmit.Click();
        }

        /// <summary>
        /// Search member of the table
        /// </summary>
        /// <param name="text"></param>
        public void SearchText(string text)
        {
            TxtSearchText.SendKeys(text);
            Label lbl = new Label(By.XPath($"//*[@class='rt-td'][1][contains(text(),'{text}')]"), driver);
            lbl.WaitForElementIsVisible(By.XPath($"//*[@class='rt-td'][1][contains(text(),'{text}')]"), driver);
        }

        public IWebElement FindFirstNameValue(string selector)
            => driver.FindElement(By.XPath($"//*[@class='rt-td'][1][contains(text(),'{selector}')]"));
        
        public IWebElement FindLastNameValue(string selector)
            => driver.FindElement(By.XPath($"//*[@class='rt-td'][2][contains(text(),'{selector}')]"));

        public IWebElement FindAgeValue(string selector)
            => driver.FindElement(By.XPath($"//*[@class='rt-td'][3][contains(text(),'{selector}')]"));

        public IWebElement FindEmailValue(string selector)
            => driver.FindElement(By.XPath($"//*[@class='rt-td'][4][contains(text(),'{selector}')]"));

        public IWebElement FindSalaryValue(string selector)
            => driver.FindElement(By.XPath($"//*[@class='rt-td'][5][contains(text(),'{selector}')]"));

        public IWebElement FindDepartmentValue(string selector)
            => driver.FindElement(By.XPath($"//*[@class='rt-td'][6][contains(text(),'{selector}')]"));

        /// <summary>
        /// Sort member of the table
        /// </summary>
        /// <returns></returns>
        public bool SortingASC()
        {
            if (Table.SortByFirstNameASC(By.ClassName("rt-table"), driver, LblFirstName))
                return true;           
            return false;
        }
        /// <summary>
        /// Edit information of member
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="age"></param>
        /// <param name="salary"></param>
        /// <param name="department"></param>
        public void EditInfo(
            string firstName,
            string lastName,
            string email,
            int age,
            double salary,
            string department)
        {
            ScrollDown(10);
            BtnEditSelector.Click();
            SendValuesToTextBox(firstName, lastName, email, age, salary, department);
            BtnSubmit.Click();
            Table.WaitForElementVisible(By.ClassName("rt-table"), driver);
        }

        /// <summary>
        /// Delete member of the table
        /// </summary>
        /// <returns></returns>
        public bool DeleteInfo()
        {
            var firstValues = driver.FindElement(By.XPath(FirstValue)).Text;
            var listBeforeDelete = Table.FindMultiElement();
            BtnDeleteSelector.Click();

            Table.WaitForElementVisible(By.ClassName("rt-table"), driver);

            var listAfterDelete = Table.FindMultiElement();

            Table.WaitForElementVisible(By.ClassName("rt-table"), driver);

            var valueAfterDelete = driver.FindElement(By.XPath(FirstValue)).Text;
            if (valueAfterDelete != FirstValue)
                return true;
            return false;
        }
        #endregion
    }
}
