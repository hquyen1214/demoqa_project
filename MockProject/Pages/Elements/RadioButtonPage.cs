﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Pages.Elements
{
    public class RadioButtonPage : BasePage
    {
        public RadioButtonPage(IWebDriver driver) : base(driver)
        {
        }
        #region Page Elements
        public RadioButton RdoNo => new RadioButton(By.Id("noRadio"), driver);
        public RadioButton RdoImpressive => new RadioButton(By.Id("impressiveRadio"), driver);
        public RadioButton RdoYes => new RadioButton(By.XPath("//input[@id='yesRadio']"), driver);
        public Textbox TxtMessageSucess => new Textbox(By.XPath("//span[@class='text-success']"), driver);
        #endregion
        #region Page Actions
        /// <summary>
        /// Select [Yes] radio button
        /// </summary>
        /// <returns></returns>
        public RadioButtonPage SelectedRadioYes()
        {
            RdoYes.ClickJavaScript();
            return this;
        }
        /// <summary>
        /// Get success text message
        /// </summary>
        /// <returns></returns>
        public string GetTextMessageSucess()
        {
            return TxtMessageSucess.Text;
        }
        /// <summary>
        /// Select [Impress] radio button
        /// </summary>
        /// <returns></returns>
        public RadioButtonPage SelectedRadioImpress()
        {
            RdoImpressive.ClickJavaScript();
            return this;
        }
        /// <summary>
        /// Check whether the [NO] radio button is enabled
        /// </summary>
        /// <returns></returns>
        public bool RadioButtonNoIsEnable()
        {
            return RdoNo.Enabled;
        }
        /// <summary>
        /// Check whether the [Yes] radio button is enabled
        /// </summary>
        /// <returns></returns>
        public bool RadioButtonYesIsEnable()
        {
            return RdoYes.Enabled;
        }
        /// <summary>
        /// Check whether the [Impressive] radio button is enabled
        /// </summary>
        /// <returns></returns>
        public bool RadioButtonImpressiveIsEnable()
        {
            return RdoImpressive.Enabled;
        }
        #endregion
    }
}

