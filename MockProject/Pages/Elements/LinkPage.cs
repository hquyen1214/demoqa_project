﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using Xunit;

namespace MockProject.Pages.Elements
{
    public class LinkPage : BasePage
    {
        public LinkPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page elements
        // new tab
        public Link LnkHomePage => new Link(By.Id("simpleLink"), driver);
        public Link LnkHomePageDynamicText => new Link(By.Id("dynamicLink"), driver);
        // api
        public Link LnkCreated => new Link(By.Id("created"), driver);
        public Link LnkNoContent => new Link(By.Id("no-content"), driver);
        public Link LnkMoved => new Link(By.Id("moved"), driver);
        public Link LnkBadRequest => new Link(By.Id("bad-request"), driver);
        public Link LnkUnauthorized => new Link(By.Id("unauthorized"), driver);
        public Link LnkForbidden => new Link(By.Id("forbidden"), driver);
        public Link LnkNotFound => new Link(By.Id("invalid-url"), driver);
        public Image ImgBanner => new Image(By.ClassName("banner-image"), driver);
        public Label TxtApiStatusCode => new Label(By.XPath("//p[@id='linkResponse']/b[1]"), driver);
        public Label TxtApiMessage => new Label(By.XPath("//p[@id='linkResponse']/b[2]"), driver);
        #endregion
        #region Page actions

        /// <summary>
        /// Click on Homepage link
        /// </summary>
        /// <returns></returns>
        public LinkPage ClickHomePageLink()
        {
            ScrollDown(200);
            LnkHomePage.ClickLink();
            return this;
        }
        /// <summary>
        /// click on dynamic link
        /// </summary>
        /// <returns></returns>
        public LinkPage ClickHomePageDynamicLink()
        {
            ScrollDown(200); 
            LnkHomePageDynamicText.ClickLink();
            return this;
        }
        /// <summary>
        /// Click on created link
        /// </summary>
        /// <returns></returns>
        public LinkPage ClickCreatedLink()
        {
            ScrollDown(200);
            LnkCreated.Click();
            return this;
        }
        /// <summary>
        /// check navigate to homepage in new tab successfully
        /// </summary>
        public bool CheckHomePageInNewTab()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.ImgBanner.Locator));
            return ImgBanner.Displayed;
        }
        /// <summary>
        /// Method get api message
        /// </summary>
        /// <returns></returns>
        public string GetApiMessage()
        {
            return TxtApiMessage.Text;
        }
        /// <summary>
        /// Method get api status code
        /// </summary>
        /// <returns></returns>
        public string GetApiStatusCode()
        {
            return TxtApiStatusCode.Text;
        }

        /// <summary>
        /// Check api call link message 201
        /// </summary>
        public void CheckLinkApiCallCreated()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TxtApiMessage.Locator));
        }
        /// <summary>
        /// Check api call link message 204
        /// </summary>
        public void CheckLinkApiCallNoContent()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TxtApiMessage.Locator));
        }
        /// <summary>
        /// Check api call link message 301
        /// </summary>
        public void CheckLinkApiCallMoved()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TxtApiMessage.Locator));
        }
        /// <summary>
        /// Check api call link message 400
        /// </summary>
        public void CheckLinkApiCallBadRequest()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TxtApiMessage.Locator));
        }
        /// <summary>
        /// Check api call link message 401
        /// </summary>
        public void CheckLinkApiCallUnauthorized()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TxtApiMessage.Locator));
        }
        /// <summary>
        /// Check api call link message 403
        /// </summary>
        public void CheckLinkApiCallForbidden()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TxtApiMessage.Locator));
        }
        /// <summary>
        /// Check api call link message 404
        /// </summary>
        public void CheckLinkApiCallNotFound()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.TxtApiMessage.Locator));
        }
        #endregion

    }
}
