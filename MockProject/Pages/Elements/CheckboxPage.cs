﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MockProject.Pages.Elements
{
    public class CheckboxPage : BasePage
    {
        public CheckboxPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Element
        public RadioButton RdoBtnExpandAll => new RadioButton(By.XPath("//button[@title='Expand all']"), driver);
        public RadioButton GetTextSuccessBtnExpandAll => new RadioButton(By.XPath("//li[@class='rct-node rct-node-parent rct-node-expanded']"), driver);
        public RadioButton GetCollapsedHome => new RadioButton(By.XPath("//li[@class='rct-node rct-node-parent rct-node-collapsed']"), driver);
        public RadioButton GetExpandedHome => new RadioButton(By.XPath("//li[@class='rct-node rct-node-parent rct-node-expanded']"), driver);
        public RadioButton RdoBtnCollapseAll => new RadioButton(By.XPath("//button[@title='Collapse all']"), driver);
        public RadioButton GetTextSuccessBtnCollapseAll => new RadioButton(By.XPath("//li[@class='rct-node rct-node-parent rct-node-collapsed']"), driver);
        public Button BtnToggle => new Button(By.XPath("//button[@title='Toggle']"), driver);
        public Button GetTextSuccessBtnToggle => new Button(By.XPath("//button[@title='Toggle']//*[name()='svg']"), driver);
        public Button GetTextSuccessBtnHome => new Button(By.XPath("//label[@for='tree-node-home']//span[@class='rct-checkbox']//*[name()='svg']"), driver);
        public DropDownList ChkHome => new DropDownList(By.XPath("//span[contains(text(),'Home')]"), driver);
        public Textbox GetTextSuccessBtn => new Textbox(By.XPath("//div[@id='result']"), driver);
        public CheckBox GetTextSuccessCheckBox => new CheckBox(By.XPath("//label[@for='tree-node-home']//span[@class='rct-checkbox']//*[name()='svg']"), driver);
        public DropDownList ChkDesktop => new DropDownList(By.XPath("//span[contains(text(),'Desktop')]"), driver);
        public DropDownList ChkExpandDesktop => new DropDownList(By.XPath("//*[contains(text(), 'Desktop')]/parent::*/preceding-sibling::*"), driver);
        public DropDownList ChkNotes => new DropDownList(By.XPath("//span[contains(text(),'Notes')]"), driver);
        public DropDownList ChkCommands => new DropDownList(By.XPath("//span[contains(text(),'Commands')]"), driver);
        public DropDownList ChkExpandDocuments => new DropDownList(By.XPath("//*[contains(text(), 'Documents')]/parent::*/preceding-sibling::*"), driver);

        #endregion

        #region Page Actions
        /// <summary>
        /// Method click in button Expand All
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickBtnExpandAll()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.RdoBtnExpandAll.Locator));
            RdoBtnExpandAll.Click();
            return this;
        }

        /// <summary>
        /// Method get message success Expand All
        /// </summary>
        /// <returns></returns>
        public string GetSuccessBtnExpandAll()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.GetTextSuccessBtnExpandAll.Locator));
            return GetTextSuccessBtnExpandAll.GetAttribute("class");
        }

        /// <summary>
        /// Method get message success Collapsed Home 
        /// </summary>
        /// <returns></returns>
        public string GetSuccessCollapsedHome()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.GetCollapsedHome.Locator));
            return GetCollapsedHome.GetAttribute("class");
        }

        /// <summary>
        /// Method get message success Expand Home
        /// </summary>
        /// <returns></returns>
        public string GetSuccessExpandedHome()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.GetExpandedHome.Locator));
            return GetExpandedHome.GetAttribute("class");
        }

        /// <summary>
        /// Method click in button Collapse All
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickBtnCollapseAll()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.RdoBtnCollapseAll.Locator));
            RdoBtnCollapseAll.Click();
            return this;
        }

        /// <summary>
        /// Method get message success Collapse All
        /// </summary>
        /// <returns></returns>
        public string GetSuccessBtnCollapseAll()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.GetTextSuccessBtnCollapseAll.Locator));
            return GetTextSuccessBtnCollapseAll.GetAttribute("class");
        }

        /// <summary>
        /// Method click in button Toggle
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickBtnToggle()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnToggle.Locator));
            BtnToggle.Click();
            return this;
        }

        /// <summary>
        /// Method click in button Home
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickChkHome()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.ChkHome.Locator));
            ChkHome.Click();
            return this;
        }

        /// <summary>
        /// Method get message success result
        /// </summary>
        /// <returns></returns>
        public string GetTextMessageSucess()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.GetTextSuccessBtn.Locator));
            return GetTextSuccessBtn.GetMessage();
        }

        /// <summary>
        /// Method get node Home
        /// </summary>
        /// <returns></returns>
        public string GetTextSuccess()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.GetTextSuccessCheckBox.Locator));
            return GetTextSuccessCheckBox.GetAttribute("class");
        }

        /// <summary>
        /// Method get node Toggle
        /// </summary>
        /// <returns></returns>
        public string GetSuccessToggle()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.GetTextSuccessBtnToggle.Locator));
            return GetTextSuccessBtnToggle.GetAttribute("class");
        }
        /// <summary>
        /// Method get Home success message
        /// </summary>
        /// <returns></returns>
        public string GetsuccessHome()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.GetTextSuccessBtnHome.Locator));
            return GetTextSuccessBtnHome.GetAttribute("class");

        }

        /// <summary>
        /// Method click in button Desktop
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickChkDesktop()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.ChkDesktop.Locator));
            ChkDesktop.Click();
            return this;
        }

        /// <summary>
        /// Method click in button Expand Desktop
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickChkExpandDesktop()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.ChkExpandDesktop.Locator));
            ChkExpandDesktop.Click();
            return this;
        }

        /// <summary>
        /// Method click in button Notes
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickChkNotes()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.ChkNotes.Locator));
            ChkNotes.Click();
            return this;
        }

        /// <summary>
        /// Method click in button Commands
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickChkCommands()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.ChkCommands.Locator));
            ChkCommands.Click();
            return this;
        }

        /// <summary>
        /// Method click in button Expand Documents
        /// </summary>
        /// <returns></returns>
        public CheckboxPage ClickChkExpandDocuments()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.ChkExpandDocuments.Locator));
            ChkExpandDocuments.Click();
            return this;
        }
        #endregion
    }
}
