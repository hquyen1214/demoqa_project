﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Pages.Elements
{
    public class ButtonPage : BasePage
    {
        public ButtonPage(IWebDriver driver) : base(driver)
        {
        }
        #region Page Elements
        public Button BtnDoubleClick => new Button(By.Id("doubleClickBtn"), driver);
        public Button BtnRightClick => new Button(By.Id("rightClickBtn"), driver);
        public Button BtnClick => new Button(By.XPath("//button[normalize-space()='Click Me']"), driver);
        public Textbox TxtdoubleClickMessage => new Textbox(By.Id("doubleClickMessage"), driver);
        public Textbox TxtrightClickMessage => new Textbox(By.Id("rightClickMessage"), driver);
        public Textbox TxtClickMessage => new Textbox(By.Id("dynamicClickMessage"), driver);
        #endregion
        #region Page Action
        /// <summary>
        /// Right click on button
        /// </summary>
        /// <returns></returns>
        public ButtonPage RightClick()
        {         
            BtnRightClick.RightClick();
            return new ButtonPage(driver);
        }
        /// <summary>
        /// Double click on button
        /// </summary>
        /// <returns></returns>
        public ButtonPage DoubleClick()
        {
            BtnDoubleClick.DoubleClick();
            return new ButtonPage(driver);
        }
        /// <summary>
        /// Able to click on button
        /// </summary>
        /// <returns></returns>
        public ButtonPage ClickOnButton()
        {
            BtnClick.Click();
            return this;
        }
        /// <summary>
        /// Get message after double click on a button
        /// </summary>
        /// <returns></returns>
        public string GetTextDoubleClickMessage()
        {
            return TxtdoubleClickMessage.Text;
        }
        /// <summary>
        /// Get message after right click on a button
        /// </summary>
        /// <returns></returns>
        public string GetTextRightClickMessage()
        {
            return TxtrightClickMessage.Text;
        }
        /// <summary>
        /// Get message after click on a button
        /// </summary>
        /// <returns></returns>
        public string GetTextClickMessage()
        {
            return TxtClickMessage.Text;
        }
        /// <summary>
        /// Check whether the button is displayed after being clicked
        /// </summary>
        /// <returns></returns>
        public bool SuccessfullyButtonClickDisplayed()
        {
            return BtnClick.Displayed;
        }
        /// <summary>
        /// Check whether the button is displayed after being double clicked
        /// </summary>
        /// <returns></returns>
        public bool SuccessfullyDoubleButtonClickDisplayed()
        {
            return BtnDoubleClick.Displayed;
        }
        /// <summary>
        /// Check whether the button is displayed after being right clicked
        /// </summary>
        /// <returns></returns>
        public bool SuccessfullyRightButtonClickDisplayed()
        {
            return BtnRightClick.Displayed;
        }
        #endregion
    }
}
