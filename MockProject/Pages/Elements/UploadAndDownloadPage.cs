using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MockProject.Pages.Elements
{
    public class UploadAndDownloadPage : BasePage
    {
        string filePath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent + @"\Images\meo.jpg";
        public UploadAndDownloadPage(IWebDriver driver) : base(driver)
        {

        }

        #region Page Elements
        public Button BtnDownload => new Button(By.CssSelector("#downloadButton"), driver);
        public Button BtnChooseFile => new Button(By.Id("uploadFile"), driver);
        public Label LblSelectAFile => new Label(By.CssSelector("label[for='uploadFile']"), driver);
        public Label LblFileUploaded => new Label(By.CssSelector("#uploadedFilePath"), driver);
        #endregion

        #region Page Actions
        /// <summary>
        /// Click on button download image
        /// </summary>
        /// <returns></returns>
        public UploadAndDownloadPage DownloadImage()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnDownload.Locator));
            BtnDownload.Click();
            return this;
        }
        /// <summary>
        /// Click on Choose file button
        /// </summary>
        /// <returns></returns>
        public UploadAndDownloadPage ChooseFile()
        {
                BtnChooseFile.ClickJavaScript();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnChooseFile.Locator));
                BtnChooseFile.ActionChooseFile(filePath);
                Thread.Sleep(1000);
                return this;
        }
        /// <summary>
        /// Get file name which user select to upload
        /// </summary>
        /// <returns></returns>
        public string GetFileNameChooseUpload()
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.LblFileUploaded.Locator));
            return LblFileUploaded.Text;
        }
       
        #endregion
    }
}
