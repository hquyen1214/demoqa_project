﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Pages.Elements
{
    public class BrokenLinkImagePage : BasePage
    {
        public BrokenLinkImagePage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements
        public Link LnkValidLink => new Link(By.XPath("//*[text()='Click Here for Valid Link']"), driver);

        public Link LnkBrokenLink => new Link(By.XPath("//*[text()='Click Here for Broken Link']"), driver);

        public Label LblStatus => new Label(By.XPath("//h3[text()='Status Codes']"), driver);

        public Image ImgBrokenImage => new Image(By.CssSelector("img[src='/images/Toolsqa_1.jpg']"), driver);
        public Image ImgValidImage => new Image(By.XPath("(//img)[3]"), driver);
        
        #endregion
        #region Page Actions
        /// <summary>
        /// Able to click on valid link
        /// </summary>
        public void ClickToValidLink()
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript("window.scrollBy(0,100)");
            Thread.Sleep(3000);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.LnkValidLink.Locator));
            LnkValidLink.Click();

        }
        /// <summary>
        /// Able to click on broken like
        /// </summary>
        public void ClickToBrokenLink()
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript("window.scrollBy(0,100)");
            Thread.Sleep(3000);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.LnkValidLink.Locator));
            LnkBrokenLink.Click();
        }
        /// <summary>
        /// Status of broken link
        /// </summary>
        /// <returns></returns>
        public string StatusOfBrokenLink()
        {
            return LblStatus.Text;
        }    
        /// <summary>
        /// Check for valid image
        /// </summary>
        /// <returns></returns>
        public bool CheckValidImage()
        {
            try
            {    
                return ImgValidImage.Displayed;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Check for broken image
        /// </summary>
        /// <returns></returns>
        public bool CheckBrokenImage()
        {
            try
            {
                return ImgBrokenImage.Displayed;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

    }
}
