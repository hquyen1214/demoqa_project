﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MockProject.Pages.Elements
{
    public class DynamicPropertiesPage : BasePage
    {
        public DynamicPropertiesPage(IWebDriver driver) : base(driver)
        {
        }
        #region Page Elements
        public Button BtnWillEnableAfter5Seconds => new Button(By.XPath("//*[@id='enableAfter']"), driver);
        public Button BtnChangeColor => new Button(By.XPath("//*[@id='colorChange']"), driver);
        public Button BtnVisibleAfter5Seconds => new Button(By.XPath("//*[@id='visibleAfter']"), driver);
        public Label LblTextRandomId => new Label(By.XPath("//button[@id='enableAfter']/preceding-sibling::*"), driver);
        #endregion
        #region Page Actions
        /// <summary>
        /// Check button is enable or not
        /// </summary>
        /// <returns></returns>
        public Boolean IsButtonEnable()
        {
            Thread.Sleep(5000);
            return BtnWillEnableAfter5Seconds.Enabled;
        }
        /// <summary>
        /// Check if the button's color is changed
        /// </summary>
        /// <returns></returns>
        public Boolean IsButtonChangeColor()
        {
            Thread.Sleep(6000);
            string color = BtnChangeColor.GetCssValue("color");
            if (color == "rgba(220, 53, 69, 1)")
            {
                return true;
            }
            else return false;
        }
        /// <summary>
        /// Check button is visible
        /// </summary>
        /// <returns></returns>
        public Boolean IsButtonVisible()
        {
            Thread.Sleep(5000);
            return BtnVisibleAfter5Seconds.Displayed;
        }
        /// <summary>
        /// Get ID of a text label
        /// </summary>
        /// <returns></returns>
        public string GetIdOfTextLabel()
        {
            return LblTextRandomId.GetAttribute("id");
        }
        /// <summary>
        /// Refresh the current page
        /// </summary>
        public void RefreshPage()
        {
            driver.Navigate().Refresh();
        }
        /// <summary>
        /// Check if the ID is a random unique number 
        /// </summary>
        /// <returns></returns>
        public Boolean IsIdRandom()
        {
            string firstId = GetIdOfTextLabel();
            RefreshPage();
            string secondId = GetIdOfTextLabel();
            if (firstId != secondId)
            {
                return true;
            }
            else return false;
        }
        #endregion
    }
}

