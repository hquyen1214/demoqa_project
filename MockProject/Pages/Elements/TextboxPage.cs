﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace MockProject.Pages.Elements
{
    public class TextboxPage : BasePage
    {
        public TextboxPage(IWebDriver driver) : base(driver)
        {
        }
        /// <summary>
        /// Page Elements
        /// </summary>
        /// <returns></returns>
        #region Page Elements
        public Textbox TxtFullName => new Textbox(By.CssSelector("#userName"), driver);
        public Textbox TxtEmail => new Textbox(By.CssSelector("#userEmail"), driver);
        public TextArea TxaCurrentAddress => new TextArea(By.XPath("//textarea[@id='currentAddress']"), driver);
        public TextArea TxaPermanentAddress => new TextArea(By.XPath("//textarea[@id='permanentAddress']"), driver);
        public Button BtnSubmit => new Button(By.CssSelector("#submit"), driver);
        public Textbox TxtFullNameOutput => new Textbox(By.CssSelector("#name"), driver);
        public Textbox TxtEmailOutput => new Textbox(By.CssSelector("#email"), driver);
        public Textbox TxtCurrentAddressOutput => new Textbox(By.XPath("//p[@id='currentAddress']"), driver);
        public Textbox TxtPermanentAddressOutput => new Textbox(By.XPath("//p[@id='permanentAddress']"), driver);
        #endregion

        #region Page Actions
        /// <summary>
        /// Method get full name
        /// </summary>
        /// <returns></returns>
        public string GetFullName()
        {
            return TxtFullNameOutput.Text;
        }
        /// <summary>
        /// Method get email
        /// </summary>
        /// <returns></returns>
        public string GetEmail()
        {
            return TxtEmailOutput.Text;
        }
        /// <summary>
        /// Method get current address
        /// </summary>
        /// <returns></returns>
        public string GetCurrentAddress()
        {
            return TxtCurrentAddressOutput.Text;
        }
        /// <summary>
        /// Method get permanent address
        /// </summary>
        /// <returns></returns>
        public string GetPermanentAddress()
        {
            return TxtPermanentAddressOutput.Text;
        }

        /// <summary>
        /// EnterDataAndSubmit()
        /// </summary>
        public TextboxPage EnterDataSubmitAndCheckOutput(string FullName, string Email, string CurrentAddress, string PermanentAddress)
        {
            try
            {             
                TxtFullName.Clear();
                TxtFullName.SendKeys(FullName);
                TxtEmail.Clear();
                TxtEmail.SendKeys(Email);
                TxaCurrentAddress.Clear();
                TxaCurrentAddress.SendKeys(CurrentAddress);
                TxaPermanentAddress.Clear();
                TxaPermanentAddress.SendKeys(PermanentAddress);
                //Scroll
                ScrollDown(200);
                BtnSubmit.Click();
                return this;
            }
            catch (Exception e)
            {
                throw (e);
            }

        }
        /// <summary>
        /// EnterWrongSyntaxEmailDataAndSubmit();
        /// </summary>
        public TextboxPage EnterDataWithWrongEmailSyntaxSubmitAndCheckOutput(string FullName,  string WrongEmail, string CurrentAddress, string PermanentAddress)
        {
            try
            {
                TxtFullName.Clear();
                TxtFullName.SendKeys(FullName);
                TxtEmail.Clear();
                TxtEmail.SendKeys(WrongEmail);
                TxaCurrentAddress.Clear();
                TxaCurrentAddress.SendKeys(CurrentAddress);
                TxaPermanentAddress.Clear();
                TxaPermanentAddress.SendKeys(PermanentAddress);
                //Scroll
                ScrollDown(200);
                BtnSubmit.Click();
                return this;     
               
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        /// <summary>
        /// Get text color
        /// </summary>
        /// <returns></returns>
        public string GetColor()
        {
            //Border color change from black to red ("1px solid rgb(255, 0, 0)")
            return TxtEmail.GetCssValue("border");
        }
        #endregion
    }
}


