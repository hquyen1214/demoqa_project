﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MockProject.Pages.Elements
{
    public class ElementsPage : BasePage
    {
        public ElementsPage(IWebDriver driver) : base(driver)
        {
        }

        #region Page Elements
        public Label LblTextbox => new Label(By.XPath("//span[text()='Text Box']"), driver);
        public Button BtnRadio => new Button(By.XPath("//span[normalize-space()='Radio Button']"), driver);
        public Button BtnButton => new Button(By.XPath("//span[normalize-space()='Buttons']"), driver);
        public Button BtnDynamicProperties => new Button(By.XPath("//span[normalize-space()='Dynamic Properties']/parent::*"), driver);
        public Label LblBrokenLinkImage => new Label(By.XPath("//span[text()='Broken Links - Images']"), driver);
        public Button BtnUploadAndDownload => new Button(By.XPath("//span[normalize-space()='Upload and Download']"), driver); ///parent::*
        public Button BtnLink => new Button(By.XPath("//span[normalize-space()='Links']"), driver);
        public Button BtnCheckbox => new Button(By.XPath("//span[normalize-space()='Check Box']"), driver);
        public Tab WebTableTab => new Tab(By.XPath("//span[normalize-space()='Web Tables']"), driver);
        #endregion
        #region Page Actions

        /// <summary>
        /// NavigateToTextboxPage
        /// </summary>
        /// <returns></returns>
        public TextboxPage NavigateToTextboxPage()
        {

            ScrollDown(200);
            LblTextbox.Click();
            return new TextboxPage(this.driver);
        }
        /// <summary>
        /// Navigate to Radio button page
        /// </summary>
        /// <returns></returns>
        public RadioButtonPage NavigateToRadioButtonPage()
        {
            ScrollDown(200);
            BtnRadio.Click();
            return new RadioButtonPage(driver);
        }
        /// <summary>
        /// Navigate to Button page
        /// </summary>
        /// <returns></returns>
        public ButtonPage NavigateToButtonPage()
        {
            ScrollDown(200);       
            BtnButton.Click();
            return new ButtonPage(driver);
        }
        /// <summary>
        /// Navigate to Dynamic Properties page
        /// </summary>
        /// <returns></returns>
        public DynamicPropertiesPage NavigateToDynamicPropertiesPage()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnDynamicProperties.Locator));
            BtnDynamicProperties.Click();
            return new DynamicPropertiesPage(driver);
        }
        /// <summary>
        /// Navigate To Broken Links - Images Page
        /// </summary>
        /// <returns></returns>
        public BrokenLinkImagePage NavigateToBrokenPage()
        {
            ScrollDown(250);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.LblBrokenLinkImage.Locator));
            LblBrokenLinkImage.Click();
            return new BrokenLinkImagePage(driver);
        }
        /// <summary>
        /// Navigate to Upload and Download Page
        /// </summary>
        /// <returns></returns>
        public UploadAndDownloadPage NavigateToUploadAndDownloadPage()
        {
            ScrollDown(400);
            BtnUploadAndDownload.Click();
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(this.BtnUploadAndDownload.Locator));
            return new UploadAndDownloadPage(driver);
        }
        /// <summary>
        /// Navigate to Link Page
        /// </summary>
        /// <returns></returns>
        public LinkPage NavigatoLinkPage()
        {
            ScrollDown(200);
            wait.Until(ExpectedConditions.ElementExists(this.BtnLink.Locator));
            BtnLink.Click();
            return new LinkPage(driver);
        }
        /// <summary>
        /// Navigate to Checkbox  Page
        /// </summary>
        /// <returns></returns>
        public CheckboxPage NavigaToCheckBoxPage()
        {
            BtnCheckbox.Click();
            return new CheckboxPage(driver);
        }
        /// <summary>
        /// Navigate to Table pages
        /// </summary>
        /// <returns></returns>
        public WebTablesPage NavigateToWebTablesPage()
        {
            ScrollDown(100);
            WebTableTab.Click();
            return new WebTablesPage(driver);
        }
        #endregion
    }
}

