﻿using CORE.ActionKeyword;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Pages
{
    public class BasePage
    {
        public IWebDriver driver;
        public WebDriverWait wait;
        public WebKeyword keyword;
        public IJavaScriptExecutor jse;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            this.wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(60));
            keyword = new WebKeyword(this.driver);
            this.wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(60));
            jse = (IJavaScriptExecutor)driver;
        }
        /// <summary>
        /// Scroll the window down 
        /// </summary>
        /// <param name="range"></param>
        public void ScrollDown(int range)
        {
            jse.ExecuteScript($"window.scrollBy(0,{range})");
        }
    }

}

