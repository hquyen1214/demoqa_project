﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using CORE.ConfigJson;
using CORE.Driver;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MockProject.Tests
{
    public class BaseTest : IDisposable
    {

        public IWebDriver driver;
        public static ExtentTest test;
        public static ExtentReports extent = new ExtentReports();
        public string Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent + @"\Report\";

        /// <summary>
        /// Init driver
        /// </summary>
        /// <returns></returns>
        public IWebDriver InitDriver()
        {
            return driver;
        }

        /// <summary>
        /// Set up driver
        /// </summary>
        public BaseTest()
        {
            // get data from config file
            JsonModel configData = JsonHelper.ReadConfigFile();
            this.driver = DriverManager.GetDriver(configData.Browser, configData);
            this.driver.Manage().Window.Maximize();
            driver.Url = configData.Url;
            Thread.Sleep(2000);
            driver.Manage().Window.Maximize();

            var htmlreporter = new ExtentHtmlReporter(Path + DateTime.Now.ToString("_MMddyyyy_hhmmtt") + ".html");
            extent.AttachReporter(htmlreporter);
        }

        
        /// <summary>
        /// Quit the driver after run tests
        /// </summary>
        public void Dispose()
        {
            extent.Flush();
            driver.Quit();
        }



    }
    
}