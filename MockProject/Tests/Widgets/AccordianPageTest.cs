﻿using FluentAssertions;
using MockProject.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Widgets
{
    public class AccordianPage : BaseTest
    {
        [Trait("Widgets", "Accordian")]
        [Fact(DisplayName = "Navigate to Accordian page")]
        public void ShouldNavigateToAccordiansPage()
        {
            test = extent.CreateTest("Navigate to Accordian page").Info("Accordian Item");
            Homepage homepage = new Homepage(driver);
            string pageTitle = homepage.NavigateToWidgetsPage()
                                        .NavigateToAccordianPage()
                                        .GetAccordianPageTitle();
            // Test case to check if the browser can navigate to AccordianPage by
            // checking the display of it's title
            pageTitle.Should().Be("Accordian");
        }

        [Trait("Widgets", "Accordian")]
        [Fact(DisplayName = "What Is Lorem Ipsum Show Informations")]
        public void ShouldWhatIsLoremIpsumShowInformations()
        {
            test = extent.CreateTest("What Is Lorem Ipsum Show Informations").Info("What Is Lorem Ipsum Show Item");
            Homepage homepage = new Homepage(driver);
            homepage.NavigateToWidgetsPage()
                    .NavigateToAccordianPage()
                    .ClickBtnWhatIsLoremIpsum();
            /*Test case to check if clicking on drop down box What Is Lorem Ipsum 
             show information by checking the display of it's information div*/
        }

        [Trait("Widgets", "Accordian")]
        [Fact(DisplayName = "Where Does It Come From Show Informations")]
        public void ShouldWhereDoesItComeFromShowInformations()
        {
            test = extent.CreateTest("Where Does It Come From Show Informations").Info("Where Does It Come From Item");
            Homepage homepage = new Homepage(driver);
            homepage.NavigateToWidgetsPage()
                    .NavigateToAccordianPage()
                    .ClickBtnWhereDoesItComeFrom();
            /*Test case to check if clicking on drop down box Where Does It Come From 
             show information by checking the display of it's information div*/
        }
        [Trait("Widgets", "Accordian")]
        [Fact(DisplayName = "Why do we use it shows Informations")]
        public void ShouldWhyDoWeUseItShowInformations()
        {
            test = extent.CreateTest("Why do we use it shows Informations").Info("Why do we use it show Item");
            Homepage homepage = new Homepage(driver);
            homepage.NavigateToWidgetsPage()
                    .NavigateToAccordianPage()
                    .ClickBtnWhyDoWeUseIt();
            /*Test case to check if clicking on drop down box Why do we use it
            show information by checking the display of it's information div*/
        }
    }
}
