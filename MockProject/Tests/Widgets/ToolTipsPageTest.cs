﻿using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Widgets
{
    public class ToolTipsPageTest : BaseTest
    {
        [Trait("Widgets", "Tool tips")]
        [Fact(DisplayName = "Should Navigate to Tool tips page")]
        public void ShouldNavigateToWidgetsToolTip()
        {
            test = extent.CreateTest("Should Navigate to Tabs page").Info("Tabs");
            Homepage homepage = new Homepage(driver);
            string pageTitle = homepage.NavigateToWidgetsPage()
                .NavigateToToolTipsPage()
                .GetPageTitle();

            pageTitle.Should().Be("Tool Tips", "Current page tittle is not Tool Tips");

        }
        [Trait("Widgets", "Tool tips")]
        [Fact (DisplayName = "Button should be hovered")]
        public void HoverButtonShouldShowToolTips()
        {
            test = extent.CreateTest("Button should be hovered").Info("Tool tips");
            Homepage homepage = new Homepage(driver);
            string toolTipsMessage = homepage.NavigateToWidgetsPage()
                .NavigateToToolTipsPage()
                .HoverButton();

            toolTipsMessage.Should().Be("You hovered over the Button", "Button not should be hovered");
        }
        [Trait("Widgets", "Tool tips")]
        [Fact(DisplayName = "Text box should be hovered")]
        public void HoverTextBoxShouldShowToolTips()
        {
            test = extent.CreateTest("Text box should be hovered").Info("Tool tips");
            Homepage homepage = new Homepage(driver);
            string toolTipsMessage = homepage.NavigateToWidgetsPage()
                .NavigateToToolTipsPage()
                .HoverTextBox();

            toolTipsMessage.Should().Be("You hovered over the text field", "Text box not should be hovered");

        }
        [Trait("Widgets", "Tool tips")]
        [Fact(DisplayName = "Contrary should be hovered")]
        public void HoverLinkContraryShouldShowToolTips()
        {
            test = extent.CreateTest("Contrary should be hovered").Info("Tool tips");
            Homepage homepage = new Homepage(driver);
            string toolTipsMessage = homepage.NavigateToWidgetsPage()
                .NavigateToToolTipsPage()
                .HoverLinkContrary();

            toolTipsMessage.Should().Be("You hovered over the Contrary", "Contrary not should be hovered");

        }
        [Trait("Widgets", "Tool tips")]
        [Fact(DisplayName = "Link 1.10.32 should be hovered")]
        public void HoverLink11032ShouldShowToolTips()
        {
            test = extent.CreateTest("Link 1.10.32 should be hovered").Info("Tool tips");
            Homepage homepage = new Homepage(driver);
            string toolTipsMessage = homepage.NavigateToWidgetsPage()
                .NavigateToToolTipsPage()
                .HoverLink11032();

            toolTipsMessage.Should().Be("You hovered over the 1.10.32", "Link 1.10.32 not should be hovered");

        }

    }
}
