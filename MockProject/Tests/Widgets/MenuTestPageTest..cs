﻿using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Widgets
{
    public class MenuTest : BaseTest
    {
        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Navigate to Menu page")]
        public void ShouldBeDisplayedMenuPageSuccessfully()
        {
            test = extent.CreateTest("Should be displayed menu page successfully").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string menuPageTitle = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .MenuText();
            //Assert
            menuPageTitle.Should().Be("Menu","Title menu page is not 'Menu'.");
        }

        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Should be highlighted Main item 1")]
        public void ShouldBeHighlightedMainItem1()
        {
            test = extent.CreateTest("Should be highlighted Main item 1").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string colorMainItem1 = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .HoverMainItem1()
                                       .MainItemColor();
            //Assert
            colorMainItem1.Should().Be("rgba(33, 37, 41, 1)", "'Main Item 1' is not highlighted.");
        }

        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Should be highlighted Main item 2")]
        public void ShouldBeHighlightedMainItem2()
        {
            test = extent.CreateTest("Should be highlighted Main item 2").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string colorMainItem2 = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .HoverMainItem2()
                                       .MainItemColor();
            colorMainItem2.Should().Be("rgba(33, 37, 41, 1)", "'Main Item 2' is not highlighted.");
            
        }

        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Should be highlighted Main item 3")]
        public void ShouldBeHighlightedMainItem3()
        {
            test = extent.CreateTest("Should be highlighted Main item 3").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string colorMainItem3 = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .HoverMainItem3()
                                       .MainItemColor();
            colorMainItem3.Should().Be("rgba(33, 37, 41, 1)", "'Main Item 3' is not highlighted.");
        }

        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Should be highlighted Sub item 1")]
        public void ShouldBeHighlightedSubItem1st()
        {
            test = extent.CreateTest("Should be highlighted Sub item 1").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string colorSubItem1 = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .HoverMainItem2()
                                       .HoverSubItem1()
                                       .SubItemColor();
            colorSubItem1.Should().Be("rgba(33, 37, 41, 1)", "The 1st 'Sub item' is not highlighted.");
        }

        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Should be highlighted Sub item 2")]
        public void ShouldBeHighlightedSubItem2nd()
        {
            test = extent.CreateTest("Should be highlighted Sub item 2").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string colorSubItem2 = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .HoverMainItem2()
                                       .HoverSubItem2()
                                       .SubItemColor();
            colorSubItem2.Should().Be("rgba(33, 37, 41, 1)", "The 2nd 'Sub item' is not highlighted.");
        }

        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Should be highlighted Sub Sub List")]
        public void ShouldBeHighlightedSubSubList()
        {
            test = extent.CreateTest("Should be highlighted Sub Sub List").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string colorSubSubList = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .HoverMainItem2()
                                       .HoverSubSubList()
                                       .SubSubItemColor();
            colorSubSubList.Should().Be("rgba(33, 37, 41, 1)", "'Sub Sub List' is not highlighted.");
        }

        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Should be highlighted Sub Sub item 1")]
        public void ShouldBeHighlightedSubSubItem1()
        {
            test = extent.CreateTest("Should be highlighted Sub Sub item 1").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string colorSubSubItem1 = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .HoverMainItem2()
                                       .HoverSubSubList()
                                       .HoverSubSubItem1()
                                       .SubSubItemColor();
            colorSubSubItem1.Should().Be("rgba(33, 37, 41, 1)", "'Sub Sub Item 1' is not highlighted.");
        }

        [Trait("Widgets", "Menu")]
        [Fact(DisplayName = "Should be highlighted Sub Sub item 2")]
        public void ShouldBeHighlightedSubSubItem2()
        {
            test = extent.CreateTest("Should be highlighted Sub Sub item 2").Info("Menu");
            Homepage homepage = new Homepage(driver);
            string colorSubSubItem2 = homepage.NavigateToWidgetsPage()
                                       .ClickBtnMenu()
                                       .HoverMainItem2()
                                       .HoverSubSubList()
                                       .HoverSubSubItem2()
                                       .SubSubItemColor();
            colorSubSubItem2.Should().Be("rgba(33, 37, 41, 1)", "'Sub Sub Item 2' is not highlighted.");
        }
    }
}
