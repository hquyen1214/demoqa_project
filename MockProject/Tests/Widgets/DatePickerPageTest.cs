﻿using FluentAssertions;
using MockProject.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Widgets
{
    public class NavigateToDatePickerPage : BaseTest
    {
        /// <summary>
        /// check that navigate to the correct requested page
        /// </summary>
        [Trait("Widgets", "Date Picker")]
        [Fact(DisplayName = "Navigate to Date Picker page")]
        public void ShouldNavigateToDatePickerPage()
        {
            test = extent.CreateTest("Navigate to Date Picker page").Info("Date Picker");
            Homepage homepage = new Homepage(driver);
            string pageTitle = homepage.NavigateToWidgetsPage()
                                       .NavigateToDatePickerPage()
                                        .GetSelectMenuPageTitle();

            pageTitle.Should().Be("Date Picker", " False title name");

        }

        /// <summary>
        /// Check: The date displayed is the same as the selected date
        /// </summary>
        [Trait("Widgets", "Date Picker")]
        [Fact(DisplayName = "Click to choose date")]
        public void ChooseDate()
        {
            test = extent.CreateTest("Click to choose date").Info("Date Picker card");
            Homepage homepage = new Homepage(driver);
            string value = homepage.NavigateToWidgetsPage()
                                       .NavigateToDatePickerPage()
                                        .GetDisplayedValueAfterClick();
            value.Should().Be("02/08/2000", " Can't choose date");
        }

        /// <summary>
        /// Check: The date and time displayed is the same as the selected date
        /// </summary>
        [Trait("Widgets", "Date Picker")]
        [Fact(DisplayName = "Click to choose date and time")]
        public void ChooseDateAndTime()
        {
            test = extent.CreateTest("Click to choose date and time").Info("Date Picker card");
            Homepage homepage = new Homepage(driver);
            string value = homepage.NavigateToWidgetsPage()
                                       .NavigateToDatePickerPage()
                                        .GetDisplayedDateAndTimeAfterClick();
            value.Should().Be("June 8, 2021 3:00 AM", " Can't choose date time");

        }

        // <summary>
        /// Check: Check that the displayed value when input character
        /// Return: The date displayed is the current time
        /// </summary>
        [Trait("Widgets", "Date Picker")]
        [Fact(DisplayName = "Check that the displayed value when input character")]
        public void InputCharacterValue()
        {
            test = extent.CreateTest("Check the displayed value").Info("Date Picker card");
            Homepage homepage = new Homepage(driver);

            string date = DateTime.Now.Date.ToString("MM/dd/yyyy");
            string value = homepage.NavigateToWidgetsPage()
                                       .NavigateToDatePickerPage()
                                        .GetDisplayedValue();
            value.Should().Be(date, " Display incorrect results");

        }

        // <summary>
        /// Check: Check that the displayed value when input value that isn't correct format
        /// Return: The date displayed is the current time
        /// </summary>
        [Trait("Widgets", "Date Picker")]
        [Fact(DisplayName = "Check that the displayed value when input error format")]
        public void InputNumValue()
        {
            test = extent.CreateTest("Check the displayed value").Info("Date Picker card");
            Homepage homepage = new Homepage(driver);
            string value = homepage.NavigateToWidgetsPage()
                                       .NavigateToDatePickerPage()
                                        .GetDisplayedNumValue();
            string input = "11/21/2021";
            value.Should().NotBe(input, " Display incorrect results");

        }

        // <summary>
        /// Check: Check that the displayed value when input value that is correct format
        /// Return: The date displayed is the current time
        /// </summary>
        [Trait("Widgets", "Date Picker")]
        [Fact(DisplayName = "Check that the displayed value when input correct format")]
        public void InputDateVaue()
        {
            test = extent.CreateTest("Check the displayed value").Info("Date Picker card");
            Homepage homepage = new Homepage(driver);
            string value = homepage.NavigateToWidgetsPage()
                                       .NavigateToDatePickerPage()
                                       .GetDisplayedDateValue();
            value.Should().Be("11/08/2021", " Display incorrect results");

        }

        // <summary>
        /// Check: Check that the date time displayed value when input character
        /// Return: The date displayed is the current time
        /// </summary>
        [Trait("Widgets", "Date Picker")]
        [Fact(DisplayName = "Check that the displayed value when input character")]
        public void InputCharacterValueInDateTime()
        {
            test = extent.CreateTest("Check the displayed value").Info("Date Picker card");
            Homepage homepage = new Homepage(driver);
            string date = DateTime.Now.ToString("MMMM dd, yyyy h:mm tt");
            string value = homepage.NavigateToWidgetsPage()
                                       .NavigateToDatePickerPage()
                                        .GetDisplayedValueInDateTime();
            value.Should().Be(date, " Display incorrect results");
        }

    }
}