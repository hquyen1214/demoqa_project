﻿using FluentAssertions;
using MockProject.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Widgets
{
    public class NavigateToWidgetsPage : BaseTest
    {
        [Trait("Widgets", "Widget Card")]
        [Fact(DisplayName = "Navigate to Widgets page")]
        public void ShouldNavigateToWidgetsPage()
        {
            test = extent.CreateTest("Navigate to widgets page").Info("Widget card");
            Homepage homepage = new Homepage(driver);
            string pageTitle = homepage.NavigateToWidgetsPage()
                                       .GetPageTitle();

            pageTitle.Should().Be("Widgets", "Current page title is not Widgets");
        }
    }
}
