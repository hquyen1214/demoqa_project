﻿using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;



namespace MockProject.Tests.Widgets
{
    public class SelectMenuPageTest : BaseTest
    {
        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Navigate to Select Menu page")]
        public void ShouldNavigateToSelectMenuPage()
        {
            test = extent.CreateTest("Navigate to Select Menu page").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            string pageTitle = homepage.NavigateToWidgetsPage()
            .NavigateToSelectMenuPage()
            .GetSelectMenuPageTitle();
            pageTitle.Should().Be("Select Menu", " False title name");
        }

        /// <summary>
        /// ThaoNP24_Check: Select Value is work properly
        /// Return selected Value 
        /// </summary>
        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Select Value is work properly")]
        public void ShouldSelectValueOnSelectMenuPage()
        {
            test = extent.CreateTest("Choose Select Value").Info("Select Menu");
            Homepage homepage = new Homepage(driver);

            string selectedValue = homepage.NavigateToWidgetsPage()
            .NavigateToSelectMenuPage()
            .GetItemOfSelectValue();

            selectedValue.Should().Be("A root option", " Can't choose item");
        }

        /// <summary>
        /// ThaoNP24_Check: Select One is work properly
        /// Return selected Value 
        /// </summary>
        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Select One is work properly")]
        public void ShouldSelectOneOnSelectMenuPage()
        {
            test = extent.CreateTest("Choose Select One").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            string selectOneTitle = homepage.NavigateToWidgetsPage()
            .NavigateToSelectMenuPage()
            .GetItemOfSelectOne();
            selectOneTitle.Should().Be("Mrs.", " Can't choose item");

        }

        /// <summary>
        /// ThaoNP24_Check: Select Style is work properly
        /// Return selected Value 
        /// </summary>
        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Select Stype is work properly")]
        public void ShouldSelectStyleOnSelectMenuPage()
        {
            test = extent.CreateTest("Choose Select Style").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            string selectedIndexValue = homepage.NavigateToWidgetsPage()
            .NavigateToSelectMenuPage()
            .GetItemOfOldStyleSelectMenu();
            selectedIndexValue.Should().Be("3", " Can't choose item");

        }

        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Select All Options into drop down")]
        public void ShouldBeMultiSelectDropDown()
        {
            test = extent.CreateTest("Select All Options").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            string options = homepage.NavigateToWidgetsPage()
                                     .ChooserSelect()
                                     .ClickMultiSelectDropDown()
                                     .SelectAllOption()
                                     .GetTextOptions();
            options.Should().Be("No options", "'Multiselect Drop Down' is not selected all options.");
        }

        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Delete all selected options")]
        public void ShouldBeDeleteAllSelectedOption()
        {
            test = extent.CreateTest("Delete all selected options").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            string options = homepage.NavigateToWidgetsPage()
                                      .ChooserSelect()
                                      .ClickMultiSelectDropDown()
                                      .SelectAllOption()
                                      .DeSelectAllOption()
                                      .GetTextTxtMultiSelectDropDown();
            options.Should().Be("Select...", "'Multiselect Drop Down' is not deleted all options.");
        }

        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Delete selected option")]
        public void ShouldBeReSelectEachOption()
        {
            test = extent.CreateTest("Delete selected option").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            string options = homepage.NavigateToWidgetsPage()
                                    .ChooserSelect()
                                    .ClickMultiSelectDropDown()
                                    .DeSelectOneOption()
                                    .GetTextTxtMultiSelect();
            options.Should().Be("Green", "'Multiselect Drop Down' is not deselect 'Blue'.");

        }

        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Choose Drag and Drop two items in standard multi")]
        public void ShoulSelectTwoItemsSuccessfully()
        {
            test = extent.CreateTest("Choose Drag and Drop two items in standard multi").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            Boolean selectedValue = homepage.NavigateToWidgetsPage().ChooserSelect().DropAndDragTwoItems().GetValue();
            //Assert.True(selectedValue);
            selectedValue.Should().BeTrue("Select value in items is not chooser standard multi");
        }

        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Choose Drag and Drop three items in standard multi")]
        public void ShoulSelectThreeItemsSuccessfully()
        {
            test = extent.CreateTest("Click two items in standard multi").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            Boolean selectedValue = homepage.NavigateToWidgetsPage().ChooserSelect().DropAndDragThreeItems().GetValue();
            selectedValue.Should().BeTrue("Select value in items is not chooser standard multi");
        }

        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Choose Drag and Drop four items in standard multi")]
        public void ShoulSelectFourItemsSuccessfully()
        {
            test = extent.CreateTest("Click four items in standard multi").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            Boolean selectedValue = homepage.NavigateToWidgetsPage().ChooserSelect().DropAndDragFourItems().GetValue();
            selectedValue.Should().BeTrue("Select value in item is not chooser standard multi");
        }

        [Trait("Widgets", "Select Menu")]
        [Fact(DisplayName = "Choose KeyBoard Ctrl items in standard multi")]
        public void ShouldSelectItemsUsingCtrlKeyBoardSuccessfully()
        {
            test = extent.CreateTest("Click KeyBoard Ctrl items in standard multi").Info("Select Menu");
            Homepage homepage = new Homepage(driver);
            Boolean selectedValue = homepage.NavigateToWidgetsPage().ChooserSelect().KeyBoardCtrl().GetValue();
            selectedValue.Should().BeTrue("Select value in items is not chooser standard multi");
        }
    }
}