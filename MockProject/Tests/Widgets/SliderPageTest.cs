﻿using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Widgets;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Widgets
{
    public class SliderPageTest : BaseTest
    {
        [Trait("Widgets", "Slider")]
        [Fact(DisplayName = "Navigate to Slider page")]
        public void ShouldAbleSlideTopage()
        {
            test = extent.CreateTest("Navigate to slider page").Info("Slider");
            Homepage homepage = new Homepage(driver);
            string priceValue = homepage.NavigateToWidgetsPage()
                    .NavigateToSliderPage()
                    .GetSliderPageTitle();
            priceValue.Should().Be("Slider", "Navigate not to slider page");
        }

        [Trait("Widgets", "Slider")]
        [Fact(DisplayName = "Should able slide to value 100")]
        public void ShouldAbleSlideToValue100()
        {
            test = extent.CreateTest("Should able slide to value 100").Info("Slider");
            Homepage homepage = new Homepage(driver);
            int priceValue = homepage.NavigateToWidgetsPage()
                    .NavigateToSliderPage()
                    .ActionsSlider100()
                    .Assert100();
            priceValue.Should().Be(100, "Should able slide not to value 100");
        }
        [Trait("Widgets", "Slider")]
        [Fact(DisplayName = "Should able slide to value 0")]
        public void ShouldAbleSlideToValue0()
        {
            test = extent.CreateTest("Should able slide to value 0").Info("Slider");
            Homepage homepage = new Homepage(driver);
            int priceValue0 = homepage.NavigateToWidgetsPage()
                    .NavigateToSliderPage()
                    .ActionsSlider0()
                    .Assert0();
            Thread.Sleep(3000);
            priceValue0.Should().Be(0, "Should able slide not to value 0");
        }
        [Trait("Widgets", "Slider")]
        [Fact(DisplayName = "Should able slide to value 50")]
        public void ShouldAbleSlideToValue50()
        {
            test = extent.CreateTest("Should able slide to value 50").Info("Slider");
            Homepage homepage = new Homepage(driver);
            int priceValue5 = homepage.NavigateToWidgetsPage()
                    .NavigateToSliderPage()
                    .ActionsSlider50()
                    .Assert50();
            priceValue5.Should().Be(50, "Should able slide not to value 50");
        }
    }
}
