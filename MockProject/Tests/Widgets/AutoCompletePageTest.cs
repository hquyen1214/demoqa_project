﻿using FluentAssertions;
using MockProject.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Widgets
{
    public class AutoCompletePageTest : BaseTest
    {
        [Trait("Widgets", "AutoComplete")]
        [Fact(DisplayName = "Navigate to autocomplete page")]
        public void ShouldNavigateToAutoCompletePage()
        {
            test = extent.CreateTest("Navigate to auto complete page").Info("AutoComplete");
            Homepage homepage = new Homepage(driver);
            string pageTitle = homepage.NavigateToWidgetsPage()
                                        .NavigateToAutoPage()
                                        .GetPageTitle();
            pageTitle.Should().Be("Auto Complete", "Navigate not to auto complete page");
        }

        [Trait("Widgets", "AutoComplete")]
        [Fact(DisplayName = "AutoComplete Multil Color")]
        public void AllowAutoCompleteMulticolor()
        {
            test = extent.CreateTest("AutoComplete Multil Color").Info("Auto Card");
            Homepage homepage = new Homepage(driver);
            string inputResult = homepage.NavigateToWidgetsPage()
                    .NavigateToAutoPage()
                    .TxbAucomplete();

            
            inputResult.Should().Be("RedBlueYellow", "Not allow AutoComplete Multil Color");

        }
        [Trait("Widgets", "AutoComplete")]
        [Fact(DisplayName = "AutoComplete autofil Multil Color")]
        public void AllowAutofilAutoCompleteMulti()
        {
            test = extent.CreateTest("AutoComplete autofil Multil Color").Info("Auto Card");
            Homepage homepage = new Homepage(driver);
            string inputResult = homepage.NavigateToWidgetsPage()
                    .NavigateToAutoPage()
                    .TxbAucompleteautofil();
            
            
            inputResult.Should().Be("RedBlueYellow", "Not allow AutoComplete autofil Multil Color");

        }
        [Trait("Widgets", "AutoComplete")]
        [Fact(DisplayName = "AutoComplete single Color")]
        public void AllowAutoCompleteSingleColor()
        {
            test = extent.CreateTest("AutoComplete single Colorr").Info("Auto Card");
            Homepage homepage = new Homepage(driver);
            string inputResult = homepage.NavigateToWidgetsPage()
                    .NavigateToAutoPage()
                    .TxbAucompletesingle();

           
            inputResult.Should().Be("Red", "Not allow AutoComplete single Colorr");

        }
        [Trait("Widgets", "AutoComplete")]
        [Fact(DisplayName = "AutoComplete autofil single Color")]
        public void AllowAutoFilAutoCompleteSinglecolor()
        {
            test = extent.CreateTest("AutoComplete autofil single Colorr").Info("Auto Card");
            Homepage homepage = new Homepage(driver);
            string inputResult = homepage.NavigateToWidgetsPage()
                    .NavigateToAutoPage()
                    .TxbAucompletesingleautofil();

            inputResult.Should().Be("Red", "Not allow AutoComplete autofil single Colorr");
        }
    }
}
