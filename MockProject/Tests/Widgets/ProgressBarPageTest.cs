﻿using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Widgets;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;


namespace MockProject.Tests.Widgets
{
    public class ProgressBarPageTest : BaseTest
    {
        [Trait("Widgets", "ProgressBar")]
        [Fact(DisplayName = "Navigate to Progress Bar page")]
        public void ShouldAbleSlideTopage()
        {
            test = extent.CreateTest("Navigate to Progress Bar page").Info("ProgressBar");
            Homepage homepage = new Homepage(driver);
            string priceValue = homepage.NavigateToWidgetsPage()
                    .NavigateToProgressBarPage()
                    .GetProgressBarPageTitle();
            priceValue.Should().Be("Progress Bar", "Navigate not to Progress Bar page");
        }
        [Trait("Widgets", "ProgressBar")]
        [Fact(DisplayName = "Navigate to Progress Bar Start page")]
        public void ProgressBarShouldRunProperly_AfterClickStartButton()
        {
            test = extent.CreateTest("Navigate to Progress Bar Start page").Info("ProgressBar");
            Homepage homepage = new Homepage(driver);
            int priceValue = homepage.NavigateToWidgetsPage()
                    .NavigateToProgressBarPage()
                    .ClickProgressBarStart()
                    .AssertStart();
            priceValue.Should().Be(100, "Navigate not to Progress Bar Start page");
        }
        [Trait("Widgets", "ProgressBar")]
        [Fact(DisplayName = "Progress Bar Should Stop After Click Stop Button")]
        public void ProgressBarShouldStop_AfterClickStopButton()
        {
            test = extent.CreateTest("Progress Bar Should Stop After Click Stop Button").Info("ProgressBar");
            Homepage homepage = new Homepage(driver);
            int priceValue = homepage.NavigateToWidgetsPage()
                    .NavigateToProgressBarPage()
                    .ClickProgressBarStop()
                    .AssertStop();
            priceValue.Should().Be(50, "Navigate not to Progress Bar Stop page");
        }

        [Trait("Widgets", "ProgressBar")]
        [Fact(DisplayName = "Progress Bar Should Reset After Click Reset Button")]
        public void ProgressBarShouldReset_AfterClickResetButton()
        {
            test = extent.CreateTest("Progress Bar Should Reset After Click Reset Button").Info("ProgressBar");
            Homepage homepage = new Homepage(driver);
            int priceValue0 = homepage.NavigateToWidgetsPage()
                    .NavigateToProgressBarPage()
                    .ClickProgressBarReset()
                    .AssertReset();
            priceValue0.Should().Be(0, "Navigate not to Progress Bar Reset page");
        }
    }
}
