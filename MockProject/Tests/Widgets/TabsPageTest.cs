﻿using FluentAssertions;
using MockProject.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests
{
    public class TabsPageTest : BaseTest
    {
        [Trait("Widgets", "Tabs")]
        [Fact (DisplayName = "Should Navigate to Tabs page")]
        public void ShouldNavigateToTabsPage()
        {
            test = extent.CreateTest("Should Navigate to Tabs page").Info("Tabs");
            Homepage homepage = new Homepage(driver);
            string pageTitle = homepage.NavigateToWidgetsPage()
                .NavigateToTabsPage()
                .GetPageTitle();

            pageTitle.Should().Be("Tabs", "Current page tittle is not Tabs");
        }
        [Trait("Widgets", "Tabs")]
        [Fact (DisplayName = "Origin Tab Should Be Clicked")]
        public void OriginShouldBeClicked()
        {
            test = extent.CreateTest("Origin Tab Should Be Clicked").Info("Tabs");
            Homepage homepage = new Homepage(driver);
            TabsPage tabsPage = new TabsPage(driver);
            homepage.NavigateToWidgetsPage().NavigateToTabsPage().ClickTabOrigin();
            
            tabsPage.TextOrigin().Should().Be(tabsPage.TextOrigin(),"Origin Tabs not should be clicked");

        }
        [Trait("Widgets", "Tabs")]
        [Fact(DisplayName = "Use Tab Should Be Clicked")]
        public void UseShouldBeClicked()
        {
            test = extent.CreateTest("Use Tab Should Be Clicked").Info("Tabs");
            Homepage homepage = new Homepage(driver);
            TabsPage tabsPage = new TabsPage(driver);
            homepage.NavigateToWidgetsPage().NavigateToTabsPage().ClickTabUse();

            tabsPage.TextUse().Should().Be(tabsPage.TextUse(), "Use Tabs not should be clicked");

        }
        [Trait("Widgets", "Tabs")]
        [Fact(DisplayName = "What Tab Should Be Clicked")]
        public void WhatShouldBeClicked()
        {
            test = extent.CreateTest("What Tab Should Be Clicked").Info("Tabs");
            Homepage homepage = new Homepage(driver);
            TabsPage tabsPage = new TabsPage(driver);
            homepage.NavigateToWidgetsPage().NavigateToTabsPage().ClickTabWhat();

            tabsPage.TextWhat().Should().Be(tabsPage.TextWhat(), "What Tabs not should be clicked");

        }
        [Trait("Widgets", "Tabs")]
        [Fact(DisplayName = "More Tab Should Be Disabled")]
        public void MoreShouldBeDisabled()
        {
            test = extent.CreateTest("More Tab Should Be Disabled").Info("Tabs");
            Homepage homepage = new Homepage(driver);
            TabsPage tabsPage = new TabsPage(driver);
            homepage.NavigateToWidgetsPage().NavigateToTabsPage().CheckTabsMoreDisabledDisplayed();
 
            tabsPage.CheckTabsMoreDisabledDisplayed().Should().Be(tabsPage.CheckTabsMoreDisabledDisplayed(), "More Tab not Should Be Disabled");

        }

    }
}
