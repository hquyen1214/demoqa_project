﻿using AventStack.ExtentReports;
using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace MockProject.Tests
{
    public class VerifyHomePageLink : BaseTest
    {
        [Trait("Elements", "Links")]
        [Fact]
        public void VerifyHomePageLinkOpenNewTab()
        {
            try
            {
                test = extent.CreateTest("Navigate to homepage in new tab ").Info("Link Test");
                Homepage homepage = new Homepage(driver);
                bool actual = homepage.NavigateToElementsPage()
                        .NavigatoLinkPage().ClickHomePageLink().CheckHomePageInNewTab();
                actual.Should().BeTrue();
                test.Log(Status.Pass);
            }
            catch (Exception e)
            {
                test.Log(Status.Fail, "Test Fail " + e.Message);
                throw;
            }


        }
        [Trait("Elements", "Links")]
        [Fact]
        public void VerifyLinkApiCallCreated()
        {
            try
            {
                test = extent.CreateTest("Api call created, respond 201 ").Info("Link Test");
                LinkPage linkPage = new LinkPage(driver);
                Homepage homepage = new Homepage(driver);
                homepage.NavigateToElementsPage()
                        .NavigatoLinkPage().ClickCreatedLink().CheckLinkApiCallCreated();
                test.Log(Status.Pass);
                string actualApiMessage = linkPage.GetApiMessage();
                string actualApiStatusCode = linkPage.GetApiStatusCode();
                actualApiMessage.Should().Be("Created");
                actualApiStatusCode.Should().Be("201");

            }
            catch (Exception e)
            {
                test.Log(Status.Fail, "Test Fail " + e.Message);
                throw;
            }


        }
    }
}
