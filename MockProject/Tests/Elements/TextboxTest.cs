﻿using AventStack.ExtentReports;
using CORE.Helper.Json;
using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Data.ClassObject;
using MockProject.Pages.Elements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests
{
    public class TextboxTest : BaseTest
    {
        [Theory]
        [Trait("Elements", "Textbox")]
        [MemberData(nameof(JsonTestData.GetTextboxData), MemberType = typeof(JsonTestData))]
        public void UserEnterAllExactlyData(TextboxClassObject info)
        {
            try
            {
                test = extent.CreateTest("User can't submit after enter exactly data to all textbox except email ").Info("Textbox Test");
                TextboxPage textboxPage = new TextboxPage(driver);
                Homepage homepage = new Homepage(driver);
                homepage.NavigateToElementsPage()
                        .NavigateToTextboxPage()
                        .EnterDataSubmitAndCheckOutput(info.FullName, info.Email, info.CurrentAddress, info.PermanentAddress);
                test.Log(Status.Pass);
                string actuaclFullName =  textboxPage.GetFullName();
                string actuaclEmail =  textboxPage.GetEmail();
                string actuaclCurrentAddress =  textboxPage.GetCurrentAddress();
                string actuaclPermanentAddress =  textboxPage.GetPermanentAddress();
                actuaclFullName.Should().Be("Name:Nguyen Thanh Tai");
                actuaclEmail.Should().Be("Email:taint31@example.com");
                actuaclCurrentAddress.Should().Be("Current Address :FPT");
                actuaclPermanentAddress.Should().Be("Permananet Address :Fsoft");
            }
            catch (Exception e)
            {
                test.Log(Status.Fail, "Test Fail " + e.Message);
                throw;
            }
        }
        [Theory]
        [Trait("Elements", "Textbox")]
        [MemberData(nameof(JsonTestData.GetTextboxData), MemberType = typeof(JsonTestData))]
        public void UserEnterAllExactlyDataExeptEmail(TextboxClassObject info)
        {
            try
            {
                test = extent.CreateTest("User can submit after enter exactly data to all textbox").Info("Textbox Test");
                Homepage homepage = new Homepage(driver);
                string result = homepage.NavigateToElementsPage()
                        .NavigateToTextboxPage()
                        .EnterDataWithWrongEmailSyntaxSubmitAndCheckOutput(info.FullName, info.WrongEmail, info.CurrentAddress, info.PermanentAddress)
                        .GetColor();
                //Border color changed from black from red
                //Assert.NotEqual("1px solid rgb(206, 212, 218, 1)", result);
                result.Should().NotBe("1px solid rgb(206, 212, 218, 1)");
                test.Log(Status.Pass, "Test Pass");
            }
            catch (Exception e)
            {
                test.Log(Status.Fail, "Test Fail " + e.Message);
                throw;
            }
        }

    }
}
