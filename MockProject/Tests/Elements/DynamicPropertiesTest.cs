﻿using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;



namespace MockProject.Tests
{
    public class DynamicPropertiesTest : BaseTest
    {
        [Trait("Elements", "Dynamic Properties")]
        [Fact]
        public void VerifyThatButtonWillEnableAfter5Seconds()
        {
            //Arrange
            Homepage homepage = new Homepage(driver);
            //Action
            bool result = homepage.NavigateToElementsPage()
            .NavigateToDynamicPropertiesPage()
            .IsButtonEnable();
            //Assert
            result.Should().BeTrue();
        }


        [Trait("Elements", "Dynamic Properties")]
        [Fact]
        public void VerifyThatButtonWillChangColorAfter5Seconds()
        {
            //Arrange
            Homepage homepage = new Homepage(driver);
            //Action
            bool result = homepage.NavigateToElementsPage()
            .NavigateToDynamicPropertiesPage()
            .IsButtonChangeColor();
            //Assert
            result.Should().BeTrue();
        }

        [Trait("Elements", "Dynamic Properties")]
        [Fact]
        public void VerifyThatButtonWillVisibleAfter5Seconds()
        {
            //Arrange
            Homepage homepage = new Homepage(driver);
            //Action
            bool result = homepage.NavigateToElementsPage()
            .NavigateToDynamicPropertiesPage()
            .IsButtonVisible();
            //Assert
            result.Should().BeTrue();
        }

        [Trait("Elements", "Dynamic Properties")]
        [Fact]
        public void VerifyThatIdOfTextIsRandom()
        {
            //Arrange
            Homepage homepage = new Homepage(driver);
            //Action
            bool result = homepage.NavigateToElementsPage()
            .NavigateToDynamicPropertiesPage()
            .IsIdRandom();
            //Assert
            result.Should().BeTrue();
        }
    }
}