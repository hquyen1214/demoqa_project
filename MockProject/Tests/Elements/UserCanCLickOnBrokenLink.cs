﻿using FluentAssertions;
using MockProject.Pages;
using MockProject.Pages.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests
{
   public class UserCanCLickOnBrokenLink : BaseTest
    {
        [Trait("Elements", "Broken Links - Images")]
        [Fact]
        public void BrokenLink()
        {
            test = extent.CreateTest("User can click on broken link").Info("Login Test");
            Homepage homepage = new Homepage(driver);
            homepage.NavigateToElementsPage()
                .NavigateToBrokenPage()
                .ClickToBrokenLink();

            BrokenLinkImagePage brokenLinkImagePage = new BrokenLinkImagePage(driver);
            string status = brokenLinkImagePage.StatusOfBrokenLink();
            status.Should().Be("Status Codes");
        }
    }
}
