﻿using FluentAssertions;
using MockProject.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Elements
{
    public class RadioButtonTest : BaseTest
    {
        [Trait("Elements", "RadioButton")]
        [Fact(DisplayName = "Check selected radio button Yes")]
        public void SuccessfullySelectedRadioYes()
        {
            test = extent.CreateTest("Radio Button Yes is selected").Info("RadioButton Test");
            Homepage homeTest = new Homepage(driver);
            string actual = homeTest.NavigateToElementsPage().NavigateToRadioButtonPage().SelectedRadioYes().GetTextMessageSucess();
            actual.Should().Be("Yes");

        }

        [Trait("Elements", "RadioButton")]
        [Fact(DisplayName = "Check selected radio button Impressive")]
        public void SuccessfullySelectedRadioImpressive()
        {
            test = extent.CreateTest("Radio Button Impressive is selected").Info("RadioButton Test");
            Homepage homeTest = new Homepage(driver);
            string actual = homeTest.NavigateToElementsPage().NavigateToRadioButtonPage().SelectedRadioImpress().GetTextMessageSucess();
            actual.Should().Be("Impressive");
        }

        [Trait("Elements", "RadioButton")]
        [Fact(DisplayName = "Check radio button No is enable")]
        public void SuccessfullyRadioButtonNoIsEnable()
        {
            test = extent.CreateTest("Check radio button No is enable").Info("RadioButton Test");
            Homepage homeTest = new Homepage(driver);
            bool actual = homeTest.NavigateToElementsPage().NavigateToRadioButtonPage().RadioButtonNoIsEnable();
            actual.Should().BeTrue("Radio button is disable");
        }



        [Trait("Elements", "RadioButton")]
        [Fact(DisplayName = "Check radio button Yes is enable")]
        public void SuccessfullyRadioButtonYesIsEnable()
        {
            test = extent.CreateTest("Check radio button Yes is enable").Info("RadioButton Test");
            Homepage homeTest = new Homepage(driver);
            bool actual = homeTest.NavigateToElementsPage().NavigateToRadioButtonPage().RadioButtonYesIsEnable();
            actual.Should().BeTrue("Radio button is disabled");
        }



        [Trait("Elements", "RadioButton")]
        [Fact(DisplayName = "Check radio button Impressive is enable")]
        public void SuccessfullyRadioButtonImpressiveIsEnable()
        {
            test = extent.CreateTest("Check radio button Impressive is enable").Info("RadioButton Test");
            Homepage homeTest = new Homepage(driver);
            bool actual = homeTest.NavigateToElementsPage().NavigateToRadioButtonPage().RadioButtonImpressiveIsEnable();
            actual.Should().BeTrue("Radio button is disable");
        }
    }
}
