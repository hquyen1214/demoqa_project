﻿using MockProject.Pages;
using MockProject.Pages.Elements;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
namespace MockProject.Tests
{
    public class DownloadTest : BaseTest
    {
        [Trait("Elements", "Upload and Download")]
        [Fact(DisplayName = "Verify that the user downloads successfully when clicking the download button")]
        public void ShouldBeDownloadSuccessfully()
        {
            // Arrange
            Homepage Homepage = new Homepage(driver);
            // Act
            var result = Homepage.NavigateToElementsPage()
                    .NavigateToUploadAndDownloadPage()
                    .DownloadImage();
            Thread.Sleep(3000);
            // Assert
            File.Exists(@"C:\Users\DELL\Downloads\sampleFile.jpeg").Should().BeTrue();
        }
    }
}
