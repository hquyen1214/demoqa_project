﻿using CORE.Elements.SimpleElement;
using FluentAssertions;
using MockProject.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests
{
   public class CheckBrokenImage : BaseTest
    {
        [Trait("Elements", "Broken Links - Images")]
        [Fact]
        public void CheckBroken()
        {
            test = extent.CreateTest("Broken Display Image").Info("Login Test");
            Homepage homepage = new Homepage(driver);
            bool check = homepage.NavigateToElementsPage()
                .NavigateToBrokenPage()
                .CheckBrokenImage();
            check.Should().BeTrue("Image is not display");
            string actualPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent + @"\Image1\" + DateTime.Now.ToString("_MMddyyyy_hhmmtt") + ".png";
            string expectedPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent + @"\Images\expected1.png";
            Thread.Sleep(3000);
            Image.TakeSnapShot(driver, actualPath);
            Image.CompareImage(actualPath, expectedPath);
        }
    }
}
