﻿using FluentAssertions;
using MockProject.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests
{
    public class UploadChooseFileTest : BaseTest
    {
        [Trait("Elements", "Upload and Download")]
        [Fact(DisplayName = "Verify that the user upload file when click Choose File")]
        public void ShouldBeUploadSuccessfullyWithChooseFileButton()
        {
            // Arrange
            Homepage HomePage = new Homepage(driver);
            // Act
            string expected = HomePage.NavigateToElementsPage()
                    .NavigateToUploadAndDownloadPage()
                    .ChooseFile()
                    .GetFileNameChooseUpload();
            Thread.Sleep(10000);
            // Assert
            expected.Should().Be("C:\\fakepath\\meo.jpg");
        }
}
    }
