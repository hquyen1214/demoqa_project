using FluentAssertions;
using MockProject.Pages;
using MockProject.Tests;
using System.Threading;
using Xunit;

namespace MockProject
{
    public class CheckboxPageTest : BaseTest
    {
        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify function button Expand All")]
        public void TestExpandAll()
        {
            Homepage homepage = new Homepage(driver);
            string message = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickBtnExpandAll()
                .GetSuccessBtnExpandAll();
            Thread.Sleep(2000);
            message.Should().Be("rct-node rct-node-parent rct-node-expanded");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify function button Collapse All")]
        public void TestCollapseAll()
        {
            Homepage homepage = new Homepage(driver);
            string message = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickBtnExpandAll()
                .ClickBtnCollapseAll()
                .GetSuccessBtnCollapseAll();
            Thread.Sleep(2000);
            message.Should().Be("rct-node rct-node-parent rct-node-collapsed");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify function button Collapse Home")]
        public void TestCollapseHome()
        {
            Homepage homepage = new Homepage(driver);
            string message = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .GetSuccessCollapsedHome();
            Thread.Sleep(2000);
            message.Should().Be("rct-node rct-node-parent rct-node-collapsed");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify function button Expanded Home")]
        public void TestExpandedHome()
        {
            Homepage homepage = new Homepage(driver);
            string message = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickBtnToggle()
                .GetSuccessExpandedHome();
            Thread.Sleep(2000);
            message.Should().Be("rct-node rct-node-parent rct-node-expanded");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify displayed check button Toggle Expand Oppen button Home")]
        public void TestCheckBtnToggle()
        {
            Homepage homepage = new Homepage(driver);
            string checkDisplayed = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickBtnToggle()
                .GetSuccessToggle();
            Thread.Sleep(2000);
            checkDisplayed.Should().Be("rct-icon rct-icon-expand-open");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify displayed un-check button Toggle Expand Close button Home")]
        public void TestReCheckBtnToggle()
        {
            Homepage homepage = new Homepage(driver);
            string checkDisplayed = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickBtnToggle()
                .ClickBtnToggle()
                .GetSuccessToggle();
            Thread.Sleep(2000);
            checkDisplayed.Should().Be("rct-icon rct-icon-expand-close");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify displayed half-check button Toggle Home")]
        public void TestHalfCheckBtnToggle()
        {
            Homepage homepage = new Homepage(driver);
            string checkDisplayed = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickChkHome()
                .ClickBtnToggle()
                .ClickChkDesktop()
                .GetsuccessHome();
            Thread.Sleep(2000);
            checkDisplayed.Should().Be("rct-icon rct-icon-half-check");
        }


        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify function click button Home")]
        public void TestChkHome()
        {
            Homepage homepage = new Homepage(driver);
            string message = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickChkHome()
                .GetTextMessageSucess();
            Thread.Sleep(2000);
            message.Should().Be("You have selected :homedesktopnotescommandsdocumentsworkspacereactangularveuofficepublicprivateclassifiedgeneraldownloadswordFileexcelFile");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify function click button Desktop")]
        public void TestChkDesktop()
        {
            Homepage homepage = new Homepage(driver);
            string message = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickBtnToggle()
                .ClickChkDesktop()
                .GetTextMessageSucess();
            Thread.Sleep(2000);
            message.Should().Be("You have selected :desktopnotescommands");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify function click button Notes")]
        public void TestChkNotes()
        {
            Homepage homepage = new Homepage(driver);
            string message = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickBtnToggle()
                .ClickChkExpandDesktop()
                .ClickChkNotes()
                .GetTextMessageSucess();
            Thread.Sleep(2000);
            message.Should().Be("You have selected :notes");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify function click button Commands")]
        public void TestChkCommands()
        {
            Homepage homepage = new Homepage(driver);
            string checkDisplayed = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickBtnToggle()
                .ClickChkExpandDesktop()
                .ClickChkCommands()
                .GetTextMessageSucess();
            Thread.Sleep(2000);
            checkDisplayed.Should().Be("You have selected :commands");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify displayed check button Toggle Home")]
        public void TestCheckBtnHome()
        {
            Homepage homepage = new Homepage(driver);
            string checkDisplayed = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickChkHome()
                .GetTextSuccess();
            Thread.Sleep(2000);
            checkDisplayed.Should().Be("rct-icon rct-icon-check");
        }

        [Trait("Elements", "Check Box")]
        [Fact(DisplayName = "Verify displayed un-check button Toggle Home")]
        public void TestReCheckBtnHome()
        {
            Homepage homepage = new Homepage(driver);
            string checkDisplayed = homepage.NavigateToElementsPage()
                .NavigaToCheckBoxPage()
                .ClickChkHome()
                .ClickChkHome()
                .GetTextSuccess();
            Thread.Sleep(2000);
            checkDisplayed.Should().Be("rct-icon rct-icon-uncheck");
        }

    }
}
