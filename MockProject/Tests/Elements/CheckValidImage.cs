﻿using CORE.Elements.SimpleElement;
using FluentAssertions;
using MockProject.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests
{
    public class CheckValidImage : BaseTest
    {
        [Trait("Elements", "Broken Links - Images")]
        [Fact]
        public void CheckValid()
        {
            test = extent.CreateTest("Valid Display Image").Info("Login Test");
            Homepage homepage = new Homepage(driver);
            bool check = homepage.NavigateToElementsPage()
                .NavigateToBrokenPage()
                .CheckValidImage();
            check.Should().BeTrue("Image is not display");
            string actualPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent + @"\Images\" + DateTime.Now.ToString("_MMddyyyy_hhmmtt") + ".png";
            string expectedPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent + @"\Images\expected.png";
            Thread.Sleep(3000);
            Image.TakeSnapShot(driver, actualPath);  
            Image.CompareImage(actualPath, expectedPath);
        }
    }
}
