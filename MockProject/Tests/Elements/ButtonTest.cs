﻿using FluentAssertions;
using MockProject.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests.Elements
{
    public class ButtonTest : BaseTest
    {

        [Trait("Elements", "Button")]
        [Fact]
        public void SuccessfullyDoubleClick()
        {
            test = extent.CreateTest("User can double click on button").Info("Button Test");
            Homepage homeTest = new Homepage(driver);
            string actual = homeTest.NavigateToElementsPage().NavigateToButtonPage().DoubleClick().GetTextDoubleClickMessage();
            actual.Should().Be("You have done a double click");
        }

        [Trait("Elements", "Button")]
        [Fact]
        public void SuccessfullyRightClick()
        {
            test = extent.CreateTest("User can right click on button").Info("Button Test");
            Homepage homeTest = new Homepage(driver);
            string actual = homeTest.NavigateToElementsPage().NavigateToButtonPage().RightClick().GetTextRightClickMessage();
            actual.Should().Be("You have done a right click");
        }

        [Trait("Elements", "Button")]
        [Fact]
        public void SuccessfullyClick()
        {
            test = extent.CreateTest("User can click on button").Info("Button Test");
            Homepage homeTest = new Homepage(driver);
            string actual = homeTest.NavigateToElementsPage().NavigateToButtonPage().ClickOnButton().GetTextClickMessage();
            actual.Should().Be("You have done a dynamic click");
        }

        [Trait("Elements", "Button")]
        [Fact]
        public void SuccessfullyDoubleClickDisplayed()
        {
            test = extent.CreateTest("Button is displayed").Info("Button Test");
            Homepage homeTest = new Homepage(driver);
            bool actual = homeTest.NavigateToElementsPage().NavigateToButtonPage().SuccessfullyButtonClickDisplayed();
            actual.Should().BeTrue("Button isn't displayed");
        }

        [Trait("Elements", "Button")]
        [Fact]
        public void SuccessfullyRightClickDisplayed()
        {
            test = extent.CreateTest("Button is displayed").Info("Button Test");
            Homepage homeTest = new Homepage(driver);
            bool actual = homeTest.NavigateToElementsPage().NavigateToButtonPage().SuccessfullyDoubleButtonClickDisplayed();
            actual.Should().BeTrue("Button isn't displayed");
        }

        [Trait("Elements", "Button")]
        [Fact]
        public void SuccessfullyClickDisplayed()
        {
            test = extent.CreateTest("Button is displayed").Info("Button Test");
            Homepage homeTest = new Homepage(driver);
            bool actual = homeTest.NavigateToElementsPage().NavigateToButtonPage().SuccessfullyRightButtonClickDisplayed();
            actual.Should().BeTrue("Button isn't displayed");
        }
    }
}
