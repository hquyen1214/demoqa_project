﻿using MockProject.Pages;
using MockProject.Pages.Elements;
using OpenQA.Selenium;
using Xunit;
using FluentAssertions;
using System;

namespace MockProject.Tests.ElementTests
{
    public class WebTablesTests : BaseTest
    {

        //public ElementsPage elementsPage;
        public Homepage homePage;

        [Trait("Elements", "Web - Table")]
        [Fact(DisplayName = "User can Add new Personal Info and verify values were inputted")]
        public void VerifyNewPersonalInfoAfterAdding()
        {
            string firstName = "Vinh";
            string lastName = "Nguyen";
            string email = "test@gmail.com";
            int age = 20;
            double salary = 10000;
            string department = "HCM";

            homePage = new Homepage(driver);

            var elementsPage = homePage.NavigateToElementsPage().NavigateToWebTablesPage();
            elementsPage.AddNewInfo(firstName, lastName, email, age, salary, department);

            elementsPage.FindFirstNameValue(firstName).Text.Should().Be(firstName);
            elementsPage.FindLastNameValue(lastName).Text.Should().Be(lastName);
            elementsPage.FindEmailValue(email).Text.Should().Be(email);
            int actualAge = int.Parse(elementsPage.FindAgeValue(age.ToString()).Text);
            actualAge.Should().Be(age);
            int actualSalary = int.Parse(elementsPage.FindSalaryValue(salary.ToString()).Text);
            actualSalary.Should().Be(Convert.ToInt32(salary));
            elementsPage.FindDepartmentValue(department).Text.Should().Be(department);
        }
        [Trait("Elements", "Web - Table")]
        [Fact(DisplayName = "User can Search by First Name and verify the value")]
        public void SearchByFirstNameAndVerifyTheValue()
        {
            string firstName = "Cierra";
            homePage = new Homepage(driver);

            var elementsPage = homePage.NavigateToElementsPage().NavigateToWebTablesPage();
            elementsPage.SearchText(firstName);
            
            elementsPage.FindFirstNameValue(firstName).Text.Should().Be(firstName);
        }
        [Trait("Elements", "Web - Table")]
        [Fact(DisplayName = "User can Sorting successfully")]
        public void Sorting()
        {
            homePage = new Homepage(driver);

            var elementsPage = homePage.NavigateToElementsPage().NavigateToWebTablesPage();

            elementsPage.SortingASC().Should().BeTrue();
        }
        [Trait("Elements", "Web - Table")]
        [Fact(DisplayName = "User can Edit info")]
        public void EditInfoAndVerifyAfterEditing()
        {
            string firstName = "Quang";
            string lastName = "Le";
            string email = "test123@gmail.com";
            int age = 20;
            double salary = 33000;
            string department = "HCM-FPT";

            homePage = new Homepage(driver);

            var elementsPage = homePage.NavigateToElementsPage().NavigateToWebTablesPage();

            elementsPage.EditInfo(firstName, lastName, email, age, salary, department);

            elementsPage.FindFirstNameValue(firstName).Text.Should().Be(firstName);
            elementsPage.FindLastNameValue(lastName).Text.Should().Be(lastName);
            elementsPage.FindEmailValue(email).Text.Should().Be(email);
            int actualAge = int.Parse(elementsPage.FindAgeValue(age.ToString()).Text);
            actualAge.Should().Be(age);
            int actualSalary = int.Parse(elementsPage.FindSalaryValue(salary.ToString()).Text);
            actualSalary.Should().Be(Convert.ToInt32(salary));
            elementsPage.FindDepartmentValue(department).Text.Should().Be(department);
        }
        [Trait("Elements", "Web - Table")]
        [Fact(DisplayName = "User can Delete info and verify after deleting")]
        public void DeleteInFoAndVerifyAfterThat()
        {

            homePage = new Homepage(driver);

            var elementsPage = homePage.NavigateToElementsPage().NavigateToWebTablesPage();
            
            elementsPage.DeleteInfo().Should().BeTrue();
        }
    }

}
