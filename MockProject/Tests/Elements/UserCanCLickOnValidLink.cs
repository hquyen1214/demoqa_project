﻿using MockProject.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MockProject.Tests
{
    public class UserCanCLickOnValidLink : BaseTest
    {
        [Trait("Elements", "Broken Links - Images")]
        [Fact]
        public void ValidLink()
        {
            test = extent.CreateTest("User can click on valid link").Info("Login Test");
            Homepage homepage = new Homepage(driver);
            homepage.NavigateToElementsPage()
                     .NavigateToBrokenPage()
                     .ClickToValidLink();
        }
    }
}
