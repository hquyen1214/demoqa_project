﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Slider : BaseElement
    {
        public Slider(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Slide the slider to the specific pixel
        /// </summary>
        /// <param name="pixel"></param>
        public void ActionSlider(int pixel)
        {
            action.DragAndDropToOffset(WrappedObject, pixel, 0).Build().Perform();
        }
        /// <summary>
        /// Get value of the element
        /// </summary>
        /// <returns></returns>
        public int GetValue()
        {
            return Convert.ToInt32((WrappedObject.GetAttribute("value")));

        }
    }
}
