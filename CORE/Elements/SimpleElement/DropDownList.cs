﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class DropDownList : BaseElement
    {
        public DropDownList(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        #region Elements
        SelectElement selectElement => new SelectElement(this.WrappedObject);
        #endregion
        /// <summary>
        /// Selector by value in drop down list
        /// </summary>
        /// <param name="value"></param>
        public void SelectByValue(string value)
        {
            Wait.Until(ExpectedConditions.ElementIsVisible(Locator));
            selectElement.SelectByValue(value);
        }
        /// <summary>
        /// Selector by text in drop down list
        /// </summary>
        /// <param name="value"></param>
        public void SelectByText(string text)
        {
            Wait.Until(ExpectedConditions.ElementIsVisible(Locator));
            selectElement.SelectByValue(text);
        }
    }
}
