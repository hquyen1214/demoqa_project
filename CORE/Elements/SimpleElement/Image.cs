﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Image : BaseElement
    {
        public Image(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Create new image with 16x16 pixel 
        /// </summary>
        /// <param name="bmpSource"></param>
        /// <returns></returns>
        public static List<bool> GetHash(Bitmap bmpSource)
        {
            List<bool> lResult = new List<bool>();
            
            Bitmap bmpMin = new Bitmap(bmpSource, new Size(16, 16));
            for (int j = 0; j < bmpMin.Height; j++)
            {
                for (int i = 0; i < bmpMin.Width; i++)
                {
                    //reduce colors to true / false
                    lResult.Add(bmpMin.GetPixel(i, j).GetBrightness() < 0.5f);
                }
            }
            return lResult;
        }
        /// <summary>
        /// Compare two image
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool CompareImage(string path1, string path)
        {
            List<bool> iHash1 = GetHash(new Bitmap(path1));
            List<bool> iHash2 = GetHash(new Bitmap(path));

            //determine the number of equal pixel (x of 256)
            int equalElements = iHash1.Zip(iHash2, (i, j) => i == j).Count(eq => eq);
            return equalElements > 10000;
        }
        /// <summary>
        /// Take screen shot
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="path"></param>
        public static void TakeSnapShot(IWebDriver driver, string path)
        {
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            ss.SaveAsFile(path,ScreenshotImageFormat.Png);
        }      

    }
}
