﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class DropAndDrag : BaseElement
    {
        public DropAndDrag(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Funtion Dragged and dropped item in Standard multi select
        /// </summary>
        /// <param name="drag1"></param>
        /// <param name="drop1"></param>
        public void DragToDrop(IWebElement drag1, IWebElement drop1)
        {
            action.ClickAndHold(drag1).Build().Perform();
            action.MoveToElement(drop1).Build().Perform();
            action.Release(drop1).Build().Perform();
        }
        /// <summary>
        /// Funtion Key Up Ctrl item in Standard multi select
        /// </summary>
        public void KeyUpCtrl()
        {
            action.KeyUp(Keys.Control).B‌uild().Perform();
        }
        /// <summary>
        /// Funtion Key Down Ctrl item in Standard multi select
        /// </summary>
        public void KeyDownCtrl()
        {
            action.KeyDown(Keys.Control).B‌uild().Perform();
        }
    }
}
