﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class ToolsTip : BaseElement
    {
        public ToolsTip(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Perform hover over an element
        /// </summary>
        public void HoverOverElement()
        {
            action.MoveToElement(WrappedObject).Perform();
        }
    }
}
