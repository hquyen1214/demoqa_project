﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Button : BaseElement
    {
        public Button(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Double click action
        /// </summary>
        public void DoubleClick()
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            action.DoubleClick(WrappedObject).Perform();
        }
        /// <summary>
        /// Right click action
        /// </summary>
        public void RightClick()
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            action.ContextClick(WrappedObject).Perform();
        }
    }
}
