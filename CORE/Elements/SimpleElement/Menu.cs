﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Menu : BaseElement
    {
        private IWebDriver driver;

        public Menu(By locator, IWebDriver driver) : base(locator, driver)
        {
            this.driver = driver;
        }

        /// <summary>
        /// Mouse hover over middle element
        /// </summary>
        public void MoveToMiddleElement()
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            Actions actions = new Actions(driver);
            actions.MoveToElement(WrappedObject).Build().Perform();
     
        }
    }
}
