﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Label : BaseElement
    {
        public Label(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Wait element to visuable and double click
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="driver"></param>
        public void WaitForElementIsVisible(By locator, IWebDriver driver)
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            Actions actions = new Actions(driver);
            actions.DoubleClick(WrappedObject).Perform();
        }
    }
}
