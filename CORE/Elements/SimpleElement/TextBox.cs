﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Textbox : BaseElement
    {
        public Textbox(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Get message text which replace all the special characters "\r\n"
        /// </summary>
        /// <returns></returns>
        public string GetMessage()
        {
            return WrappedObject.Text.Replace("\r\n", "");    
        }
    }
}
