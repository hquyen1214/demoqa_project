﻿//using AutoItX3Lib;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Threading;
namespace CORE.Elements.SimpleElement
{
    public class BaseElement
    {
        public Actions action;
        public IWebDriver Driver;
        public By Locator;
        public WebDriverWait Wait;
        public IJavaScriptExecutor js;

        public BaseElement(By locator, IWebDriver driver)
        {
            this.Driver = driver;
            this.Locator = locator;
            Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
            js = (IJavaScriptExecutor)Driver;
            action = new Actions(driver);
        }
        public IWebElement WrappedObject => Driver.FindElement(Locator);
        public string TagName => WrappedObject.TagName;
        public string Text => WrappedObject.Text;
        public bool Enabled => WrappedObject.Enabled;
        public bool Selected => WrappedObject.Selected;
        public Point Location => WrappedObject.Location;
        public Size Size => WrappedObject.Size;
        public bool Displayed => WrappedObject.Displayed;
        /// <summary>
        /// Check whether the element is disable or not
        /// </summary>
        public string IsDisable()
        {
            Wait.Until(ExpectedConditions.ElementExists(Locator));
            return WrappedObject.GetAttribute("disabled");
        }
        /// <summary>
        /// Perform click action on that element
        /// </summary>
        public void Click()
        {

            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            WrappedObject.Click();
        }
        /// <summary>
        /// Perform find element by id, classname, xpath, ...
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public IWebElement FindElement(By by)
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            return WrappedObject.FindElement(by);
        }
        /// <summary>
        /// Perform find elements by id, classname, xpath, ...
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            return WrappedObject.FindElements(by);
        }
        /// <summary>
        /// Perform clear action
        /// </summary>
        public void Clear()
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            WrappedObject.Clear();
        }
        /// <summary>
        /// Perform set text action
        /// </summary>
        /// <param name="text"></param>
        public void SendKeys(string text)
        {
            Wait.Until(ExpectedConditions.ElementExists(Locator));
            WrappedObject.SendKeys(text);
        }
        /// <summary>
        /// Get elememt attribute
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public string GetAttribute(string attributeName)
        {
            Wait.Until(ExpectedConditions.ElementExists(Locator));
            return WrappedObject.GetAttribute(attributeName);
        }
        /// <summary>
        /// Get element css value
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public string GetCssValue(string propertyName)
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            return WrappedObject.GetCssValue(propertyName);
        }
        /// <summary>
        /// Get element property
        /// </summary>
        /// <returns></returns>
        public void ClickJavaScript()
        {
            js.ExecuteScript("arguments[0].click();", WrappedObject);
        }
        /// <summary>
        /// Get element property
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public string GetProperty(string propertyName)
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            return WrappedObject.GetDomProperty(propertyName);
        }
        /// <summary>
        /// Perform submit action
        /// </summary>
        public void Submit()
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            WrappedObject.Submit();
        }

        // Action choose file
        public void ActionChooseFile(string filePath)
        {
            //AutoItX3 autoIt = new AutoItX3();
            //autoIt.WinActivate("Open");
            //autoIt.Send(filePath);
            //Thread.Sleep(3000);
            //autoIt.Send("{ENTER}");
        }
        
    }
}
