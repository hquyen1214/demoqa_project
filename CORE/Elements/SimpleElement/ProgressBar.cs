﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class ProgressBar : BaseElement
    {
        public ProgressBar(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Get current value of the progress bar
        /// </summary>
        /// <returns></returns>
        public int GetValueProgressBar()
        {
            return Convert.ToInt32((WrappedObject.GetAttribute("aria-valuenow")));
        }
    }
}
