﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Link : BaseElement
    {
        public Link(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// custom click method
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="driver"></param>
        public void ClickLink()
        {
            Wait.Until(ExpectedConditions.ElementExists(Locator));
            WrappedObject.Click();
            _ = this.Driver.SwitchTo().Window(this.Driver.WindowHandles.Last());
        }
        /// <summary>
        /// return href in element
        /// </summary>
        /// <returns>url</returns>
        public string GetLink()
        {
            Wait.Until(ExpectedConditions.ElementExists(Locator));
            return WrappedObject.GetAttribute("href");
        }

    }
}