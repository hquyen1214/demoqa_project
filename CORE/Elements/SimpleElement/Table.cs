﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Table : BaseElement
    {
        
        public Table(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Create Element list
        /// </summary>
        /// <returns></returns>
        public IList<IWebElement> FindMultiElement()
             => Driver.FindElements(By.XPath($"//*[@class='rt-td'][1]"));

        public void WaitForElementVisible(By locator, IWebDriver driver)
        {
            Wait.Until(ExpectedConditions.ElementExists(locator));
        }

        /// <summary>
        /// Sort by first name after user click on [FirstName] of table
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="driver"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public bool SortByFirstNameASC(By locator, IWebDriver driver, string selector)
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            var listBeforeSort = FindMultiElement();

            var listAfterSort = FindMultiElement();

            string[] convertListBefore = new string[listBeforeSort.Count];
            int i = 0;
            foreach (IWebElement element in listBeforeSort)
            {
                convertListBefore[i++] = element.Text;
            }

            var lblFirstName = driver.FindElement(By.XPath(selector));
            lblFirstName.Click();

            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));

            string[] convertListAfter = new string[listAfterSort.Count];
            int j = 0;
            foreach (IWebElement element in listAfterSort)
            {
                convertListAfter[j++] = element.Text;
            }

            if (convertListBefore.Equals(convertListAfter))
                return false;
            return true;
        }
        
        /// <summary>
        /// Click on button
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="driver"></param>
        public void ClickingOnButton(By locator, IWebDriver driver)
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            Actions actions = new Actions(driver);
            actions.Click(WrappedObject).Perform();
        }
    }
}
