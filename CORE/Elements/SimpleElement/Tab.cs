﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.SimpleElement
{
    public class Tab : BaseElement
    {
        public Tab(By locator, IWebDriver driver) : base(locator, driver)
        {
        }
        /// <summary>
        /// Check whether text are displayed are not
        /// </summary>
        /// <returns></returns>
        public bool TextDisplayed()
        {
            Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(Locator));
            return WrappedObject.Displayed;
        }

    }
}