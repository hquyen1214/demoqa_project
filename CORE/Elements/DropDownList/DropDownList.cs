﻿using CORE.Elements.SimpleElement;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Elements.DropDownList
{
    public class DropDownList : BaseElement
    {
        public DropDownList(By locator, IWebDriver driver) : base(locator, driver)
        {
        }

        public void SelectByValue(string value)
        {
            this.WrappedObject.Click();
            By locator = By.XPath($"//li[text() = '{value}']");
            this.Wait.Until(ExpectedConditions.ElementIsVisible(locator));
            this.Driver.FindElement(locator).Click();
        }
    }
}
