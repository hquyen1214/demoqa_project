﻿using CORE.ConfigJson;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Driver
{
    public class DriverManager
    {
        /// <summary>
        /// Custom driver base on config file
        /// </summary>
        /// <param name="browser">browser want to run on</param>
        /// <param name="configData">config data read from json file</param>
        /// <returns></returns>
        public static IWebDriver GetDriver(string browser, JsonModel configData)
        {
            switch (browser)
            {
                case "chrome":
                    var options = new ChromeOptions();
                    if (configData.Incognito) options.AddArgument("--incognito");
                    if (configData.MaximizeBrowser) options.AddArgument("start-maximized");
                    return new ChromeDriver(options);
                case "ff":
                    FirefoxOptions ffoptions = new FirefoxOptions();
                    if (configData.Incognito) ffoptions.AddArgument("-private");
                    FirefoxDriver ffDriver = new FirefoxDriver(ffoptions);
                    if (configData.MaximizeBrowser) ffDriver.Manage().Window.Maximize();
                    return ffDriver;
                default:
                    return new ChromeDriver();
            }
        }
    }
}
