﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.ConfigJson
{
    public class JsonHelper
    {
        /// <summary>
        /// Read data from config file and serialize as Json class
        /// </summary>
        /// <returns></returns>
        public static JsonModel ReadConfigFile()
        {
            string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent + @"\config.json";
            JsonModel json;
            using (StreamReader file = File.OpenText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                json = (JsonModel)serializer.Deserialize(file, typeof(JsonModel));
            }
            return json;
        }
    }
}
