﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.ConfigJson
{
    public class JsonModel
    {
        public string Browser { get; set; }
        public bool Incognito { get; set; }
        public bool MaximizeBrowser { get; set; }
        public string Url { get; set; }
    }
}
