﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Helper.Json
{
    public class JsonHelper
    {
        /// <summary>
        /// Read data from an object in json file
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="filePath">file path</param>
        /// <param name="objectClass">object class</param>
        /// <returns></returns>
        public static object ReadJsonDataFromFile<T>(string filePath, T objectClass)
        {

            using (StreamReader file = File.OpenText(filePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                T objectClassData = (T)serializer.Deserialize(file, typeof(T));
                return objectClassData;
            }

        }

        /// <summary>
        /// Read data from an obj array in json file
        /// </summary>
        /// <typeparam name="T">generic type</typeparam>
        /// <param name="filePath">file path</param>
        /// <param name="objectClass">object class</param>
        /// <returns></returns>
        public static IEnumerable<object[]> ReadJsonDataFromFiles<T>(string filePath, T objectClass)
        {

            using (StreamReader file = File.OpenText(filePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                List<T> objectClassData = (List<T>)serializer.Deserialize(file, typeof(List<T>));
                foreach (dynamic obj in objectClassData ?? Enumerable.Empty<T>())
                {
                    yield return new[] { obj };
                }
            }

        }
    }
}