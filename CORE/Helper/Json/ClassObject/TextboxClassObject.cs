﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Pages.Data.ClassObject
{
    public class TextboxClassObject
    {
        public string FullName { get; set;}
        public string Email { get; set; }
        public string WrongEmail { get; set;}
        public string CurrentAddress { get; set;}
        public string PermanentAddress { get; set; }

    }
}
