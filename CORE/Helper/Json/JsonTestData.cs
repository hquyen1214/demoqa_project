﻿using MockProject.Pages.Data.ClassObject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Helper.Json
{
    public static class JsonTestData
    {
        public static IEnumerable<object[]> GetTextboxData => JsonHelper.ReadJsonDataFromFiles(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent + @"\Data\textboxdata.json", new TextboxClassObject());
    }
}
