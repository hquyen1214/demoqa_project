﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.ActionKeyword
{
    public class WebKeyword
    {
        private IWebDriver driver;
        private WebDriverWait wait;
        public WebKeyword(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(60));
        }

        /// <summary>
        /// Check before start open url
        /// </summary>
        /// <param name="url">url of test site</param>
        public void OpenUrl(String url)
        {
            if (!(url.StartsWith("http://") || url.StartsWith("https://")))
            {
                throw new Exception("Url is missing protocal");
            }
            this.driver.Url = url;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        public void Navigate(String url)
        {
            if (!(url.StartsWith("http://") || url.StartsWith("https://")))
            {
                throw new Exception("Url is missing protocal");
            }
            this.driver.Navigate().GoToUrl(url);
        }

        /// <summary>
        /// Find element in the website
        /// </summary>
        /// <param name="by">method to find element</param>
        /// <returns></returns>
        public IWebElement FindElement(By by)
        {
            return wait.Until(ExpectedConditions.ElementExists(by));
        }

        /// <summary>
        /// Action click on website element
        /// </summary>
        /// <param name="elem">website element</param>
        public void Click(IWebElement elem)
        {
            Actions _action = new Actions(this.driver);
            _action.MoveToElement(elem).Build().Perform();
            elem.Click();
        }
        /// <summary>
        /// Set text for the website element
        /// </summary>
        /// <param name="element">website element</param>
        /// <param name="text">Text want to set into the element</param>
        public void SetText(IWebElement element, string text)
        {
            try
            {
                element.Clear();
                element.SendKeys(text);
            }
            catch (WebDriverException e)
            {
                throw new WebDriverException("Element is not enable for set text" + "\r\n" + "error: " + e.Message);
            }

        }
        /// <summary>
        /// Set value for combo box
        /// </summary>
        /// <param name="element">website element</param>
        /// <param name="type">type of select element</param>
        /// <param name="options">option want to pass base on select type</param>
        public void Select(IWebElement element, SelectType type, string options)
        {
            SelectElement select = new SelectElement(element);
            switch (type)
            {
                case SelectType.SelectByIndex:
                    try
                    {
                        select.SelectByIndex(Int32.Parse(options));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.GetBaseException().ToString());
                        throw new ArgumentException("Please input numberic on selectOption for SelectType.SelectByIndex");
                    }
                    break;
                case SelectType.SelectByText:
                    select.SelectByText(options);
                    break;
                case SelectType.SelectByValue:
                    select.SelectByValue(options);
                    break;
                default:
                    throw new Exception("Get error in using Selected");
            }
        }
    }
}

public enum SelectType
{
    SelectByIndex,
    SelectByText,
    SelectByValue,
}